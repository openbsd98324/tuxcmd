/*  Tux Commander VFS: String utilities
 *  Copyright (C) 2007 Tomas Bzatek <tbzatek@users.sourceforge.net>
 *   Check for updates on tuxcmd.sourceforge.net
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __STRUTILS_H__
#define __STRUTILS_H__



#include <string.h>
#include <stdlib.h>


#ifdef __VERBOSE_DEBUG
  #define log(msg...) printf(msg)
#else
  #define log(msg...)  { }
#endif

#define IS_DIR_SEP(ch) ((ch) == '/')



//  path manipulating functions, all return newly allocated string, you can then free them as you want
char* include_trailing_path_sep(const char *APath);
char* exclude_trailing_path_sep(const char *APath);
char* include_leading_path_sep(const char *APath);
char* exclude_leading_path_sep(const char *APath);
char* extract_file_name(const char *APath);
char* extract_file_path(const char *APath);
char* resolve_relative(const char *source, const char *point_to);
char* canonicalize_filename(const char *filename);

char* wide_to_utf8(const wchar_t *src);

#endif /* __STRUTILS_H__ */
