/*  Tux Commander VFS: basic tree_path routines 
 *  Copyright (C) 2007 Tomas Bzatek <tbzatek@users.sourceforge.net>
 *   Check for updates on tuxcmd.sourceforge.net
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __TREEPATH_VFS_H__
#define __TREEPATH_VFS_H__


#include <stdio.h>
#include <string.h>
#include <glib.h>

#include "vfs_types.h"
#include "treepathutils.h"


struct VfsFilelistData {
	struct PathTree *files;

	unsigned int list_dir_index;
	struct PathTree *list_dir_node;
	
	gboolean break_get_dir_size;
};


struct VfsFilelistData* vfs_filelist_new(struct PathTree *files);
void vfs_filelist_free(struct VfsFilelistData *data);
void vfs_filelist_set_files(struct VfsFilelistData *data, struct PathTree *files);

u_int64_t vfs_filelist_get_dir_size(struct VfsFilelistData *data, char *APath);
void vfs_filelist_get_dir_size_break(struct VfsFilelistData *data);

long vfs_filelist_file_exists(struct VfsFilelistData *data, const char *FileName, const long Use_lstat);
TVFSResult vfs_filelist_file_info(struct VfsFilelistData *data, char *AFileName, struct TVFSItem *Item);

TVFSResult vfs_filelist_list_first(struct VfsFilelistData *data, char *sDir, struct TVFSItem *Item);
TVFSResult vfs_filelist_list_next(struct VfsFilelistData *data, char *sDir, struct TVFSItem *Item);
TVFSResult vfs_filelist_list_close(struct VfsFilelistData *data);

char* vfs_filelist_change_dir(struct VfsFilelistData *data, char *NewPath);




#endif /* __TREEPATH_VFS_H__ */
