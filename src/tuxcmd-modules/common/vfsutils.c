/*  Tux Commander VFS: String utilities
 *  Copyright (C) 2007 Tomas Bzatek <tbzatek@users.sourceforge.net>
 *   Check for updates on tuxcmd.sourceforge.net
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>
#include <string.h>
#include <glib.h>

#include "strutils.h"
#include "vfs_types.h"





void copy_vfs_item(struct TVFSItem *src, struct TVFSItem *dst)
{
  dst->FName = g_strdup (src->FName);
  dst->FDisplayName = g_strdup (src->FDisplayName);
  dst->iSize = src->iSize;
  dst->iPackedSize = src->iPackedSize;
  dst->m_time = src->m_time;
  dst->a_time = src->a_time;
  dst->c_time = src->c_time;
  dst->iMode = src->iMode;
  dst->sLinkTo = g_strdup (src->sLinkTo);
  dst->iUID = src->iUID;
  dst->iGID = src->iGID;
  dst->ItemType = src->ItemType;
}

void free_vfs_item(struct TVFSItem *item)
{
  if (item) {
    if (item->FName)
      free(item->FName);
    if (item->FDisplayName)
      free(item->FDisplayName);
    if (item->sLinkTo)
      free(item->sLinkTo);
    free(item);
  }
}

int compare_two_same_files(const char *Path1, const char *Path2)
{
  char *f1 = exclude_trailing_path_sep(Path1);
  char *f2 = exclude_trailing_path_sep(Path2);
  gboolean b = strcmp(f1, f2) == 0;
  free(f1);
  free(f2);
  return b;
}
