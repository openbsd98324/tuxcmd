libarchive plugin for Tux Commander
  Version: 0.1.5
  Release date: 2009-Oct-25

Copyright (C) 2008-2009 Tomas Bzatek <tbzatek@users.sourceforge.net>
http://tuxcmd.sourceforge.net

This plugin uses libarchive library v2.5.5
Copyright (c) 2003-2007 Tim Kientzle
http://people.freebsd.org/~kientzle/libarchive/
See libarchive-2.5.5/COPYING for licensing conditions



This is the libarchive archiving VFS (Virtual File System) plugin 
for Tux Commander file manager. Uses libarchive library, providing access
and routines for manipulation with the archives.

By default, Tux Commander binary package comes with statically linked
libarchive plugin, not requiring any libraries other than zlib and bzip2,
which are pretty standard in modern linux distributions.


Feature highlights:
  *  read-only support for TAR, TAR.GZ, TAR.BZ2, ISO9660, CPIO, A (ar), 
     DEB archives
  *  full support for Unix file and directory permissions
  *  large files (> 4GB) support
  *  correctly handles sparse files

To be implemented:
  *  archive consistency testing (needs new VFS API)
  *  write support (probably creating new archives only)
  
Requirements:
  - system zlib and bzip2 libraries


Static compilation (default):
  - requires uudecode utility (sharutils package)  
  - "make static"
  - libz and libbz2 are still linked dynamically
  - libarchive sources are included in plugin source tree, no need 
    for any external files
    
Shared library compilation:
  - requires libarchive library installed on your system
  - "make shared"

Compilation has been tested with gcc compiler version 4.4.2
