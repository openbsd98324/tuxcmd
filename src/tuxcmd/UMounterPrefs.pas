(*
    Tux Commander - UMounterPrefs - Mounter Preferences dialog
    Copyright (C) 2004 Tomas Bzatek <tbzatek@users.sourceforge.net>
    Check for updates on tuxcmd.sourceforge.net

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
unit UMounterPrefs;

interface

uses
  glib2, gdk2, gtk2, pango, SysUtils, Types, Classes, Variants, GTKControls, GTKForms, GTKStdCtrls, GTKExtCtrls, GTKConsts,
  GTKView, GTKUtils, GTKDialogs, GTKPixbuf, GTKClasses, GTKMenus,    
  UCoreClasses, UGnome;

type
  TFMounterPrefs = class(TGTKDialog)
    TitleFrame: TGTKFrame;
    TitleLabel: TGTKLabel;
    TitleEventBox: TGTKEventBox;
    TitleIcon: TGTKImage;
    TitleHBox: TGTKHBox;
    ListView: TGTKListView;
    ListViewScrolledWindow: TGTKScrolledWindow;
    ListViewFrame, PropertiesFrame: TGTKFrame;
    MounterListTable, PropertiesTable: TGTKTable;
    AddItemButton, RemoveItemButton: TGTKButton;
    MoveUpButton, MoveDownButton: TGTKImageButton;
    HBox1: TGTKHBox;
    DisplayTextLabel, MountPointLabel, MountDeviceLabel, MountCommandLabel, UmountCommandLabel, IconLabel, DeviceTypeLabel: TGTKLabel;
    DisplayTextEntry, MountPointEntry, MountDeviceEntry, MountCommandEntry, UmountCommandEntry: TGTKEntry;
    IconEntry: TGnomeIconEntry;
    IconEntry2: TGTKButton;
    DeviceTypeOptionMenu: TGTKOptionMenu;
    miLocalDisk, miRemovable, miCD, miFloppy, miNetwork: TGTKMenuItem;
    UseFSTabDefaultsCheckBox: TGTKCheckButton;
    ToggleModeCheckBox: TGTKCheckButton;
    procedure FormCreate(Sender: TObject); override;
    procedure FormDestroy(Sender: TObject);
    procedure ListViewSelectionChanged(Sender: TObject);
    procedure UseFSTabDefaultsCheckBoxToggled(Sender: TObject);
    procedure AddItemButtonClick(Sender: TObject);
    procedure RemoveItemButtonClick(Sender: TObject);
    procedure MoveUpDownButtonButtonClick(Sender: TObject);
    procedure DisplayTextEntryChanged(Sender: TObject);
    procedure MountPointEntryChanged(Sender: TObject);
    procedure MountDeviceEntryChanged(Sender: TObject);
    procedure MountCommandEntryChanged(Sender: TObject);
    procedure UmountCommandEntryChanged(Sender: TObject);
    procedure DeviceTypeOptionMenuChanged(Sender: TObject);
    procedure IconEntryChanged(Sender: TObject);
    procedure IconEntry2Click(Sender: TObject);
  private
    FUseGnomeIconEntry: boolean;
    procedure RefreshIcon(ItemIndex: integer);
  public
    InternalMounterList: TList;
    procedure AssignAssocList(List: TList);
    procedure FillList;
    procedure CleanItems;
  end;

var
  FMounterPrefs: TFMounterPrefs;

implementation

uses ULocale, UFileAssoc, UCoreUtils, UConfig, UCore;

function TGTKListView_drag_end(widget: PGtkWidget; drag_context: PGdkDragContext; user_data: gpointer): gboolean; cdecl; forward;

procedure TFMounterPrefs.FormCreate(Sender: TObject);
const row_targets: TGtkTargetEntry = (target:'GTK_TREE_MODEL_ROW'; flags:GTK_TARGET_SAME_APP; info:100);
var Column: TGTKTreeViewColumn;
begin
  FUseGnomeIconEntry := Assigned(gnome_icon_entry_new) and Assigned(gnome_icon_entry_get_filename) and Assigned(gnome_icon_entry_set_filename);
//  FUseGnomeIconEntry := False;
  SetDefaultSize(500, 520);
  Caption := LANGMounterPrefs_Caption;
  Buttons := [mbOK, mbCancel];
  ShowSeparator := False;
  TitleEventBox := TGTKEventBox.Create(Self);
  TitleLabel := TGTKLabel.Create(Self);
  TitleLabel.Caption := Format('<span size="x-large" weight="ultrabold">%s</span>', [LANGMounterPrefs_TitleLabelCaption]); 
  TitleLabel.UseMarkup := True;
  TitleLabel.XAlign := 0;
  TitleLabel.XPadding := 0;
  TitleLabel.YPadding := 3;
  TitleEventBox.ControlState := csPrelight;
  TitleFrame := TGTKFrame.CreateWithoutLabel(Self);
  TitleFrame.ShadowType := stShadowOut;
  TitleIcon := TGTKImage.Create(Self);
  TitleIcon.SetFromStock('gtk-cdrom', isLargeToolbar);
  TitleHBox := TGTKHBox.Create(Self);
  TitleHBox.Homogeneous := False;
  TitleHBox.AddControlEx(TGTKEventBox.Create(Self), False, False, 5);
  TitleHBox.AddControlEx(TitleIcon, False, False, 0);
  TitleHBox.AddControlEx(TitleLabel, True, True, 10);
  TitleEventBox.AddControl(TitleHBox);
  TitleFrame.AddControl(TitleEventBox);
  ClientArea.AddControlEx(TitleFrame, False, True, 0);

  ListViewFrame := TGTKFrame.Create(Self);
  ListViewFrame.Caption := LANGMounterPrefs_ListViewFrameCaption;
  ListViewFrame.BorderWidth := 7;
  ClientArea.AddControlEx(ListViewFrame, True, True, 0);
  MounterListTable := TGTKTable.Create(Self);
  ListViewFrame.AddControl(MounterListTable);
  MounterListTable.BorderWidth := 7;

  ListView := TGTKListView.CreateTyped(Self, False, [lcText, lcText, lcText, lcPointer, lcPixbuf]);
  g_signal_connect(ListView.FWidget, 'drag-end', G_CALLBACK(@TGTKListView_drag_end), nil);
  gtk_tree_view_enable_model_drag_source(GTK_TREE_VIEW(ListView.FWidget), GDK_BUTTON1_MASK, @row_targets, 1, GDK_ACTION_MOVE or GDK_ACTION_COPY);
  gtk_tree_view_enable_model_drag_dest(GTK_TREE_VIEW(ListView.FWidget), @row_targets, 1, GDK_ACTION_MOVE or GDK_ACTION_COPY);
  ListView.RulesHint := True;
//  ListView.Reorderable := True;
  Column := ListView.Columns.AddTyped(ctImageText);
  Column.Caption := LANGMounterPrefs_MountName;
  Column.AddImageAttribute('pixbuf', 4);
  Column.AddAttribute('text', 0);
  Column.Resizable := True;
  Column.FixedWidth := 150;
  Column.SizingMode := smFixed;
//  Column.Clickable := True;
  Column := ListView.Columns.Add;
  Column.Caption := LANGMounterPrefs_MountPoint;
  Column.AddAttribute('text', 1);
  Column.Resizable := True;
  Column.FixedWidth := 200;
  Column.SizingMode := smFixed;
//  Column.Clickable := True;
  Column := ListView.Columns.Add;
  Column.Caption := LANGMounterPrefs_Device;
  Column.AddAttribute('text', 2);
  Column.Resizable := True;
  Column.FixedWidth := 150;
  Column.SizingMode := smFixed;
//  Column.Clickable := True;
  ListViewScrolledWindow := TGTKScrolledWindow.Create(Self);
  ListViewScrolledWindow.AddControl(ListView);
  ListViewScrolledWindow.HorizScrollBarPolicy := sbAutomatic;
  ListViewScrolledWindow.VertScrollBarPolicy := sbAutomatic;
  ListViewScrolledWindow.ShadowType := stShadowIn;

  AddItemButton := TGTKButton.CreateFromStock(Self, 'gtk-add');
  RemoveItemButton := TGTKButton.CreateFromStock(Self, 'gtk-remove');
  HBox1 := TGTKHBox.Create(Self);
  HBox1.Homogeneous := True;
  HBox1.AddControlEx(AddItemButton, True, True, 2);
  HBox1.AddControlEx(RemoveItemButton, True, True, 2);
  MoveUpButton := TGTKImageButton.CreateWithoutLabel(Self);
  MoveUpButton.SetFromStock('gtk-go-up', isSmallToolbar);
  MoveUpButton.Tooltip := LANGMounterPrefs_MoveUpButtonTooltip;
  MoveUpButton.CanFocus := False;
  MoveDownButton := TGTKImageButton.CreateWithoutLabel(Self);
  MoveDownButton.SetFromStock('gtk-go-down', isSmallToolbar);
  MoveDownButton.Tooltip := LANGMounterPrefs_MoveDownButtonTooltip;
  MoveDownButton.CanFocus := False;

  UseFSTabDefaultsCheckBox := TGTKCheckButton.CreateWithLabel(Self, LANGMounterPrefs_UseFSTabDefaultsCheckBox);
  UseFSTabDefaultsCheckBox.Tooltip := LANGMounterPrefs_UseFSTabDefaultsCheckBoxTooltip;
  ToggleModeCheckBox := TGTKCheckButton.CreateWithLabel(Self, LANGMounterPrefs_ToggleModeCheckBox);
  ToggleModeCheckBox.Tooltip := LANGMounterPrefs_ToggleModeCheckBoxTooltip;

  MounterListTable.AddControlEx(0, 0, 3, 1, UseFSTabDefaultsCheckBox, [taoExpand, taoFill], [taoShrink, taoFill], 0, 2);
  MounterListTable.AddControlEx(0, 1, 3, 1, ToggleModeCheckBox, [taoExpand, taoFill], [taoShrink, taoFill], 0, 2);
  MounterListTable.AddControlEx(0, 2, 3, 4, ListViewScrolledWindow, [taoExpand, taoFill], [taoExpand, taoFill], 0, 5);
  MounterListTable.AddControlEx(0, 6, 1, 1, TGTKLabel.Create(Self), [taoExpand, taoFill], [taoShrink, taoFill], 0, 2);
  MounterListTable.AddControlEx(2, 6, 1, 1, HBox1, [taoShrink, taoFill], [taoShrink, taoFill], 0, 2);
  MounterListTable.AddControlEx(3, 3, 1, 1, MoveUpButton, [taoShrink, taoFill], [taoShrink], 5, 5);
  MounterListTable.AddControlEx(3, 4, 1, 1, MoveDownButton, [taoShrink, taoFill], [taoShrink], 5, 5);
  MounterListTable.AddControlEx(3, 2, 1, 1, TGTKLabel.Create(Self), [taoShrink, taoFill], [taoExpand, taoFill], 0, 2);
  MounterListTable.AddControlEx(3, 5, 1, 1, TGTKLabel.Create(Self), [taoShrink, taoFill], [taoExpand, taoFill], 0, 2);

  PropertiesFrame := TGTKFrame.Create(Self);
  PropertiesFrame.Caption := LANGMounterPrefs_PropertiesFrameCaption;
  PropertiesFrame.BorderWidth := 7;
  PropertiesTable := TGTKTable.Create(Self);
  PropertiesFrame.AddControl(PropertiesTable);
  ClientArea.AddControlEx(PropertiesFrame, False, False, 0);
  DisplayTextLabel := TGTKLabel.Create(Self);
  DisplayTextLabel.Caption := LANGMounterPrefs_DisplayTextLabelCaption;
  DisplayTextLabel.XAlign := 0;
  DisplayTextLabel.UseUnderline := True;
  MountPointLabel := TGTKLabel.Create(Self);
  MountPointLabel.Caption := LANGMounterPrefs_MountPointLabelCaption;
  MountPointLabel.XAlign := 0;
  MountPointLabel.UseUnderline := True;
  MountDeviceLabel := TGTKLabel.Create(Self);
  MountDeviceLabel.Caption := LANGMounterPrefs_MountDeviceLabelCaption;
  MountDeviceLabel.XAlign := 0;
  MountDeviceLabel.UseUnderline := True;
  DeviceTypeLabel := TGTKLabel.Create(Self);
  DeviceTypeLabel.Caption := LANGMounterPrefs_DeviceTypeLabelCaption;
  DeviceTypeLabel.XAlign := 0;
  DeviceTypeLabel.UseUnderline := True;
  DisplayTextEntry := TGTKEntry.Create(Self);
  DisplayTextLabel.FocusControl := DisplayTextEntry;
  MountPointEntry := TGTKEntry.Create(Self);
  MountPointLabel.FocusControl := MountPointEntry;
  MountDeviceEntry := TGTKEntry.Create(Self);
  MountDeviceLabel.FocusControl := MountDeviceEntry;
  DeviceTypeOptionMenu := TGTKOptionMenu.Create(Self);
  DeviceTypeLabel.FocusControl := DeviceTypeOptionMenu;
  miLocalDisk := TGTKMenuItem.CreateTyped(Self, itImageText);
  miLocalDisk.Caption := LANGMounterPrefs_miLocalDiskCaption;
  miLocalDisk.Icon := MounterHDD;
  miRemovable := TGTKMenuItem.CreateTyped(Self, itImageText);
  miRemovable.Caption := LANGMounterPrefs_miRemovableCaption;
  miRemovable.Icon := MounterRemovable;
  miCD := TGTKMenuItem.CreateTyped(Self, itImageText);
  miCD.Caption := LANGMounterPrefs_miCDCaption;
  miCD.Icon := MounterCD;
  miFloppy := TGTKMenuItem.CreateTyped(Self, itImageText);
  miFloppy.Caption := LANGMounterPrefs_miFloppyCaption;
  miFloppy.Icon := MounterFloppy;
  miNetwork := TGTKMenuItem.CreateTyped(Self, itImageText);
  miNetwork.Caption := LANGMounterPrefs_miNetworkCaption;
  miNetwork.Icon := MounterNetwork;
  DeviceTypeOptionMenu.Items.Add(miLocalDisk);
  DeviceTypeOptionMenu.Items.Add(miRemovable);
  DeviceTypeOptionMenu.Items.Add(miCD);
  DeviceTypeOptionMenu.Items.Add(miFloppy);
  DeviceTypeOptionMenu.Items.Add(miNetwork);

  MountCommandLabel := TGTKLabel.Create(Self);
  MountCommandLabel.Caption := LANGMounterPrefs_MountCommandLabelCaption;
  MountCommandLabel.XAlign := 0;
  MountCommandLabel.UseUnderline := True;
  UmountCommandLabel := TGTKLabel.Create(Self);
  UmountCommandLabel.Caption := LANGMounterPrefs_UmountCommandLabelCaption;
  UmountCommandLabel.XAlign := 0;
  UmountCommandLabel.UseUnderline := True;
  MountCommandEntry := TGTKEntry.Create(Self);
  MountCommandEntry.Tooltip := LANGMounterPrefs_MountCommandEntryTooltip;
  MountCommandLabel.FocusControl := MountCommandEntry;
  UmountCommandEntry := TGTKEntry.Create(Self);
  UmountCommandEntry.Tooltip := LANGMounterPrefs_UmountCommandEntryTooltip;
  UmountCommandLabel.FocusControl := UmountCommandEntry;
  PropertiesTable.AddControlEx(0, 0, 1, 1, DisplayTextLabel, [taoShrink, taoFill], [taoShrink, taoFill], 10, 2);
  PropertiesTable.AddControlEx(1, 0, 2, 1, DisplayTextEntry, [taoExpand, taoFill], [taoShrink, taoFill], 10, 2);
  PropertiesTable.AddControlEx(0, 1, 1, 1, MountPointLabel, [taoShrink, taoFill], [taoShrink, taoFill], 10, 2);
  PropertiesTable.AddControlEx(1, 1, 2, 1, MountPointEntry, [taoExpand, taoFill], [taoShrink, taoFill], 10, 2);
  PropertiesTable.AddControlEx(0, 2, 1, 1, MountDeviceLabel, [taoShrink, taoFill], [taoShrink, taoFill], 10, 2);
  PropertiesTable.AddControlEx(1, 2, 1, 1, MountDeviceEntry, [taoExpand, taoFill], [taoShrink, taoFill], 10, 2);
  PropertiesTable.AddControlEx(2, 2, 1, 1, DeviceTypeLabel, [taoShrink, taoFill], [taoShrink, taoFill], 0, 2);
  PropertiesTable.AddControlEx(3, 2, 2, 1, DeviceTypeOptionMenu, [taoExpand, taoFill], [taoShrink, taoFill], 10, 2);
  PropertiesTable.AddControlEx(0, 3, 1, 1, MountCommandLabel, [taoShrink, taoFill], [taoShrink, taoFill], 10, 2);
  PropertiesTable.AddControlEx(1, 3, 1, 1, MountCommandEntry, [taoExpand, taoFill], [taoShrink, taoFill], 10, 2);
  PropertiesTable.AddControlEx(2, 3, 1, 1, UmountCommandLabel, [taoShrink, taoFill], [taoShrink, taoFill], 0, 2);
  PropertiesTable.AddControlEx(3, 3, 2, 1, UmountCommandEntry, [taoExpand, taoFill], [taoShrink, taoFill], 10, 2);

  IconLabel := TGTKLabel.Create(Self);
  IconLabel.Caption := LANGMounterPrefs_IconLabelCaption;
  IconLabel.XAlign := 1;
  IconLabel.UseUnderline := True;
  PropertiesTable.AddControlEx(3, 0, 1, 2, IconLabel, [taoExpand, taoFill], [taoShrink, taoFill], 0, 2);
  if FUseGnomeIconEntry then begin
    IconEntry := TGnomeIconEntry.Create(Self);
    IconLabel.FocusControl := IconEntry;
    PropertiesTable.AddControlEx(4, 0, 1, 2, IconEntry, [taoExpand, taoFill], [taoExpand, taoFill], 0, 0);
  end else begin
    IconEntry2 := TGTKButton.Create(Self);
    IconEntry2.Caption := LANGBrowseButton_Caption;
    PropertiesTable.AddControlEx(4, 0, 1, 2, IconEntry2, [taoExpand, taoFill], [taoExpand, taoFill], 10, 4);
  end;



  OnDestroy := FormDestroy;
  ListView.OnSelectionChanged := ListViewSelectionChanged;
  UseFSTabDefaultsCheckBox.OnToggled := UseFSTabDefaultsCheckBoxToggled;
  AddItemButton.OnClick := AddItemButtonClick;
  RemoveItemButton.OnClick := RemoveItemButtonClick;
  MoveUpButton.OnClick := MoveUpDownButtonButtonClick;
  MoveDownButton.OnClick := MoveUpDownButtonButtonClick;
  DisplayTextEntry.OnChanged := DisplayTextEntryChanged;
  MountPointEntry.OnChanged := MountPointEntryChanged;
  MountDeviceEntry.OnChanged := MountDeviceEntryChanged;
  MountCommandEntry.OnChanged := MountCommandEntryChanged;
  UmountCommandEntry.OnChanged := UmountCommandEntryChanged;
  DeviceTypeOptionMenu.OnChanged := DeviceTypeOptionMenuChanged;
  if FUseGnomeIconEntry then IconEntry.OnIconChanged := IconEntryChanged
                        else IconEntry2.OnClick := IconEntry2Click;

  ListViewSelectionChanged(Self);
  ListView.SetFocus;
end;

procedure TFMounterPrefs.ListViewSelectionChanged(Sender: TObject);
var b: boolean;
    Item: TMounterItem;
begin
  b := Assigned(ListView.Selected) and Assigned(ListView.Selected.AsPointer(3)) and ListViewScrolledWindow.Enabled;
  RemoveItemButton.Enabled := b;
  MoveUpButton.Enabled := b and (ListView.Selected.Index > 0);
  MoveDownButton.Enabled := b and (ListView.Selected.Index < ListView.Items.Count - 1);
  PropertiesTable.Enabled := b;
  if not b then begin
    DisplayTextEntry.Text := '';
    MountPointEntry.Text := '';
    MountDeviceEntry.Text := '';
    MountCommandEntry.Text := '';
    UmountCommandEntry.Text := '';
    DeviceTypeOptionMenu.ItemIndex := 0;
    if FUseGnomeIconEntry then IconEntry.Filename := '';
    Exit;
  end;
  Item := ListView.Selected.AsPointer(3);
  DisplayTextEntry.Text := Item.DisplayText;
  MountPointEntry.Text := StrToUTF8(Item.MountPath);
  MountDeviceEntry.Text := StrToUTF8(Item.Device);
  MountCommandEntry.Text := StrToUTF8(Item.MountCommand);
  UmountCommandEntry.Text := StrToUTF8(Item.UmountCommand);
  DeviceTypeOptionMenu.ItemIndex := Item.DeviceType;
  if FUseGnomeIconEntry then IconEntry.Filename := Item.IconPath;
end;

procedure TFMounterPrefs.UseFSTabDefaultsCheckBoxToggled(Sender: TObject);
begin
  ListViewScrolledWindow.Enabled := not UseFSTabDefaultsCheckBox.Checked;
  AddItemButton.Enabled := not UseFSTabDefaultsCheckBox.Checked;
  RemoveItemButton.Enabled := not UseFSTabDefaultsCheckBox.Checked;
  MoveUpButton.Enabled := not UseFSTabDefaultsCheckBox.Checked;
  MoveDownButton.Enabled := not UseFSTabDefaultsCheckBox.Checked;
  ListViewSelectionChanged(Self);
end;

procedure TFMounterPrefs.AddItemButtonClick(Sender: TObject);
var Item: TMounterItem;
    ListItem: TGTKListItem;
begin
  Item := TMounterItem.Create;
  InternalMounterList.Add(Item);
  ListItem := ListView.Items.Add;
  ListItem.SetValue(3, Item);
  ListItem.Selected := True;
  ListItem.SetCursor(0, False, not Application.GTKVersion_2_2_0_Up, 0.5, 0);
  ListViewSelectionChanged(Self);
  DisplayTextEntry.SetFocus;
  RefreshIcon(ListView.Selected.Index);
end;

procedure TFMounterPrefs.RemoveItemButtonClick(Sender: TObject);
var Item: TMounterItem;
begin
  if Assigned(ListView.Selected) and Assigned(ListView.Selected.AsPointer(3)) then begin
    Item := ListView.Selected.AsPointer(3);
    Item.Free;
    InternalMounterList.Remove(Item);
    ListView.Items.Delete(ListView.Selected.Index);
  end;
end;

procedure TFMounterPrefs.MoveUpDownButtonButtonClick(Sender: TObject);
var i, Old, New: integer;
    s: string;
    p: pointer;
begin
  if Assigned(ListView.Selected) and Assigned(ListView.Selected.AsPointer(3)) then begin
    Old := InternalMounterList.IndexOf(ListView.Selected.AsPointer(3));
    if (Sender = MoveUpButton) and (Old > 0) then New := Old - 1 else
    if (Sender = MoveDownButton) and (Old < InternalMounterList.Count - 1) then New := Old + 1 else Exit;
    InternalMounterList.Move(Old, New);
    for i := 0 to 2 do begin
      s := ListView.Items[New].AsString(i);
      ListView.Items[New].SetValue(i, ListView.Items[Old].AsString(i));
      ListView.Items[Old].SetValue(i, s);
    end;
    for i := 3 to 4 do begin
      p := ListView.Items[New].AsPointer(i);
      ListView.Items[New].SetValue(i, ListView.Items[Old].AsPointer(i));
      ListView.Items[Old].SetValue(i, p);
    end;
    ListView.Items[New].Selected := True;
    ListView.Items[New].SetCursor(0, False, not Application.GTKVersion_2_2_0_Up, 0.5, 0);
  end;
end;

procedure TFMounterPrefs.DisplayTextEntryChanged(Sender: TObject);
begin
  if Assigned(ListView.Selected) and Assigned(ListView.Selected.AsPointer(3)) and (not UseFSTabDefaultsCheckBox.Checked) then begin
    TMounterItem(ListView.Selected.AsPointer(3)).DisplayText := DisplayTextEntry.Text;
    ListView.Selected.SetValue(0, DisplayTextEntry.Text);
  end;
end;

procedure TFMounterPrefs.MountPointEntryChanged(Sender: TObject);
begin
  if Assigned(ListView.Selected) and Assigned(ListView.Selected.AsPointer(3)) and (not UseFSTabDefaultsCheckBox.Checked) then begin
    TMounterItem(ListView.Selected.AsPointer(3)).MountPath := UTF8ToStr(MountPointEntry.Text);
    ListView.Selected.SetValue(1, MountPointEntry.Text);
  end;
end;

procedure TFMounterPrefs.MountDeviceEntryChanged(Sender: TObject);
begin
  if Assigned(ListView.Selected) and Assigned(ListView.Selected.AsPointer(3)) and (not UseFSTabDefaultsCheckBox.Checked) then begin
    TMounterItem(ListView.Selected.AsPointer(3)).Device := UTF8ToStr(MountDeviceEntry.Text);
    ListView.Selected.SetValue(2, MountDeviceEntry.Text);
  end;
end;

procedure TFMounterPrefs.MountCommandEntryChanged(Sender: TObject);
begin
  if Assigned(ListView.Selected) and Assigned(ListView.Selected.AsPointer(3)) and (not UseFSTabDefaultsCheckBox.Checked) then begin
    TMounterItem(ListView.Selected.AsPointer(3)).MountCommand := UTF8ToStr(MountCommandEntry.Text);
  end;
end;

procedure TFMounterPrefs.UmountCommandEntryChanged(Sender: TObject);
begin
  if Assigned(ListView.Selected) and Assigned(ListView.Selected.AsPointer(3)) and (not UseFSTabDefaultsCheckBox.Checked) then begin
    TMounterItem(ListView.Selected.AsPointer(3)).UmountCommand := UTF8ToStr(UmountCommandEntry.Text);
  end;
end;

procedure TFMounterPrefs.DeviceTypeOptionMenuChanged(Sender: TObject);
begin
  if Assigned(ListView.Selected) and Assigned(ListView.Selected.AsPointer(3)) and (not UseFSTabDefaultsCheckBox.Checked) then begin
    TMounterItem(ListView.Selected.AsPointer(3)).DeviceType := DeviceTypeOptionMenu.ItemIndex;
    RefreshIcon(ListView.Selected.Index);
  end;
end;

procedure TFMounterPrefs.IconEntryChanged(Sender: TObject);
begin
  if Assigned(ListView.Selected) and Assigned(ListView.Selected.AsPointer(3)) and (not UseFSTabDefaultsCheckBox.Checked) then begin
    TMounterItem(ListView.Selected.AsPointer(3)).IconPath := IconEntry.Filename;
    RefreshIcon(ListView.Selected.Index);
  end;
end;

procedure TFMounterPrefs.RefreshIcon(ItemIndex: integer);
var Pixmap: TGDKPixbuf;
    b: boolean;
    Data: TMounterItem;
begin
  Data := InternalMounterList[ItemIndex];
  b := FileExists(Data.IconPath);
  Pixmap := nil;
  if b then begin
    Pixmap := TGDKPixbuf.Create(Self);
    Pixmap.LoadFromFile(Data.IconPath);
    b := Pixmap.FPixbuf <> nil;
    if b then Pixmap.ScaleSimple(16, 16);
  end;
  if not b then
    case Data.DeviceType of
      0 : Pixmap := MounterHDD;
      1 : Pixmap := MounterRemovable;
      2 : Pixmap := MounterCD;
      3 : Pixmap := MounterFloppy;
      4 : Pixmap := MounterNetwork;
    end;
  ListView.Items[ItemIndex].SetValue(4, Pixmap.FPixbuf);
end;

procedure TFMounterPrefs.IconEntry2Click(Sender: TObject);
var Dialog: TGTKFileSelectionDialog;
begin
  if (not Assigned(ListView.Selected)) or (not Assigned(ListView.Selected.AsPointer(3))) then Exit;
  Dialog := TGTKFileSelectionDialog.CreateWithTitle(Self, LANGBrowseForIcon);
  try
    Dialog.ShowFileOpButtons := False;
    Dialog.FileName := TMounterItem(ListView.Selected.AsPointer(3)).IconPath;
    if Byte(Dialog.Run) = 251 then TMounterItem(ListView.Selected.AsPointer(3)).IconPath := Dialog.FileName;
    RefreshIcon(ListView.Selected.Index);
  finally
    Dialog.Free;
  end;
end;

(********************************************************************************************************************************)
(********************************************************************************************************************************)
procedure TFMounterPrefs.AssignAssocList(List: TList);
var i: integer;
    Item: TMounterItem;
begin
  InternalMounterList := TList.Create;
  if List.Count > 0 then
    for i := 0 to List.Count - 1 do begin
      Item := TMounterItem.Create;
      with Item do begin
        DisplayText := TMounterItem(List[i]).DisplayText;
        MountPath := TMounterItem(List[i]).MountPath;
        Device := TMounterItem(List[i]).Device;
        IconPath := TMounterItem(List[i]).IconPath;
        MountCommand := TMounterItem(List[i]).MountCommand;
        UmountCommand := TMounterItem(List[i]).UmountCommand;
        DeviceType := TMounterItem(List[i]).DeviceType;
      end;
      InternalMounterList.Add(Item);
    end;
end;

procedure TFMounterPrefs.FillList;
var i: integer;
    ListItem: TGTKListItem;
begin
  if Assigned(InternalMounterList) and (InternalMounterList.Count > 0) then begin
    for i := 0 to InternalMounterList.Count - 1 do
      with TMounterItem(InternalMounterList[i]) do begin
        ListItem := ListView.Items.Add;
        ListItem.SetValue(0, DisplayText);
        ListItem.SetValue(1, StrToUTF8(MountPath));
        ListItem.SetValue(2, StrToUTF8(Device));
        ListItem.SetValue(3, InternalMounterList[i]);
        RefreshIcon(i);
      end;
  end;
  UseFSTabDefaultsCheckBox.Checked := ConfMounterUseFSTab;
  ToggleModeCheckBox.Checked := ConfMounterPushDown;
end;

procedure TFMounterPrefs.CleanItems;
var i: integer;
begin
  try
  if InternalMounterList.Count > 0 then
    for i := InternalMounterList.Count - 1 downto 0 do
      with TMounterItem(InternalMounterList[i]) do begin
        if Length(Trim(Device)) = 0 then begin
          TMounterItem(InternalMounterList[i]).Free;
          InternalMounterList.Delete(i);
        end;
      end;
  except end;
end;

procedure TFMounterPrefs.FormDestroy(Sender: TObject);
var i: integer;
begin
  if Assigned(InternalMounterList) then begin
    if InternalMounterList.Count > 0 then
      for i := 0 to InternalMounterList.Count - 1 do
        if Assigned(InternalMounterList[i]) then TMounterItem(InternalMounterList[i]).Free;
    InternalMounterList.Clear;
    InternalMounterList.Free;
  end;
end;

function TGTKListView_drag_end(widget: PGtkWidget; drag_context: PGdkDragContext; user_data: gpointer): gboolean; cdecl; 
var i: integer;
begin
  Result := False;
  for i := 0 to FMounterPrefs.ListView.Items.Count - 1 do
    FMounterPrefs.InternalMounterList[i] := FMounterPrefs.ListView.Items[i].AsPointer(3);
end;
(********************************************************************************************************************************)
end.

