(*
    Tux Commander - UOverwrite - Overwrite dialog form and related funcions
    Copyright (C) 2004 Tomas Bzatek <tbzatek@users.sourceforge.net>
    Check for updates on tuxcmd.sourceforge.net

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
unit UOverwrite;

interface

uses
  SysUtils, Types, Classes, Variants, GTKControls, GTKForms, GTKStdCtrls, GTKExtCtrls, GTKConsts;

type
  TFOverwrite = class(TGTKDialog)
    FromLabel, FromInfoLabel, ToLabel, ToInfoLabel: TGTKLabel;
    ButtonTable: TGTKTable;
    FromVBox, ToVBox: TGTKVBox;
    OverwriteButton, OverwriteAllButton, SkipButton, CancelButton, OverwriteAllOlderButton, SkipAllButton, RenameButton, AppendButton : TGTKButton;
    procedure FormCreate(Sender: TObject); override;
    procedure FormKeyDown(Sender: TObject; Key: Word; Shift: TShiftState; var Accept: boolean);
    procedure ButtonClick(Sender: TObject);
  public
    RenameStr, SourceFile, DestFile: string;
    procedure AddButtons(Sel: integer);
    procedure DoRename;
    procedure DoAppend;
  end;

var
  FOverwrite: TFOverwrite;

implementation

uses ULocale, UCopyMove;


procedure TFOverwrite.FormCreate(Sender: TObject);
begin
  Caption := LANGOverwriteQuestion;
  FromLabel := TGTKLabel.Create(Self);
  FromLabel.XAlign := 0;    FromLabel.XPadding := 10;
  FromInfoLabel := TGTKLabel.Create(Self);
  FromInfoLabel.XAlign := 0;   FromInfoLabel.XPadding := 40;
  ToLabel := TGTKLabel.Create(Self);
  ToLabel.XAlign := 0;    ToLabel.XPadding := 10;
  ToInfoLabel := TGTKLabel.Create(Self);
  ToInfoLabel.XAlign := 0;    ToInfoLabel.XPadding := 40;
  OverwriteButton := TGTKButton.Create(Self);
  OverwriteButton.Caption := LANGOverwriteButton_Caption;
  OverwriteButton.OnClick := ButtonClick;
  OverwriteAllButton := TGTKButton.Create(Self);
  OverwriteAllButton.Caption := LANGOverwriteAllButton_Caption;
  OverwriteAllButton.OnClick := ButtonClick;
  SkipButton := TGTKButton.Create(Self);
  SkipButton.Caption := LANGSkipButton_Caption;
  SkipButton.OnClick := ButtonClick;
  CancelButton := TGTKButton.Create(Self);
  CancelButton.Caption := LANGCancel;
  CancelButton.OnClick := ButtonClick;
  OverwriteAllOlderButton := TGTKButton.Create(Self);
  OverwriteAllOlderButton.Caption := LANGOverwriteAllOlderButton_Caption;
  OverwriteAllOlderButton.OnClick := ButtonClick;
  SkipAllButton := TGTKButton.Create(Self);
  SkipAllButton.Caption := LANGSkipAllButton_Caption;
  SkipAllButton.OnClick := ButtonClick;
  RenameButton := TGTKButton.Create(Self);
  RenameButton.Caption := LANGRenameButton_Caption;
  RenameButton.OnClick := ButtonClick;
  AppendButton := TGTKButton.Create(Self);
  AppendButton.Caption := LANGAppendButton_Caption;
  AppendButton.OnClick := ButtonClick;
  ButtonTable := TGTKTable.Create(Self);
  ButtonTable.RowCount := 3;
  ButtonTable.ColCount := 3;
  ButtonTable.RowSpacing := 3;
  ButtonTable.ColSpacing := 3;
  ButtonTable.Homogeneous := True;
  FromVBox := TGTKVBox.Create(Self);
  FromVBox.AddControlEx(FromLabel, True, True, 0);
  FromVBox.AddControlEx(FromInfoLabel, True, True, 0);
  ToVBox := TGTKVBox.Create(Self);
  ToVBox.AddControlEx(ToLabel, True, True, 0);
  ToVBox.AddControlEx(ToInfoLabel, True, True, 0);
  ClientArea.AddControlEx(FromVBox, True, True, 5);
  ClientArea.AddControlEx(ToVBox, True, True, 5);
  ActionArea.AddControlEx(ButtonTable, True, True, 0);
  OnKeyDown := FormKeyDown;
end;

procedure TFOverwrite.FormKeyDown(Sender: TObject; Key: Word; Shift: TShiftState; var Accept: boolean);
begin
  case Key of
    GDK_O : ModalResult := TMessageButton(1);
    GDK_A : ModalResult := TMessageButton(2);
    GDK_S : ModalResult := TMessageButton(3);
    GDK_C, GDK_ESCAPE : ModalResult := TMessageButton(4);
    GDK_D : ModalResult := TMessageButton(5);
    GDK_K : ModalResult := TMessageButton(6);
    GDK_R : DoRename;
    GDK_P : DoAppend;
  end;
end;

procedure TFOverwrite.AddButtons(Sel: integer);
begin
  ButtonTable.AddControl(0, 0, 1, 1, OverwriteButton, 0, 0);
  ButtonTable.AddControl(1, 0, 1, 1, OverwriteAllButton, 0, 0);
  ButtonTable.AddControl(2, 0, 1, 1, SkipButton, 0, 0);
  ButtonTable.AddControl(0, 1, 1, 1, CancelButton, 0, 0);
  ButtonTable.AddControl(1, 1, 1, 1, OverwriteAllOlderButton, 0, 0);
  ButtonTable.AddControl(2, 1, 1, 1, SkipAllButton, 0, 0);
  ButtonTable.AddControl(0, 2, 1, 1, RenameButton, 0, 0);
  case Sel of
    2 : ButtonTable.AddControl(1, 2, 1, 1, AppendButton, 0, 0);
  end;
end;

procedure TFOverwrite.ButtonClick(Sender: TObject);
begin
  if Sender = OverwriteButton then ModalResult := TMessageButton(1) else
  if Sender = OverwriteAllButton then ModalResult := TMessageButton(2) else
  if Sender = SkipButton then ModalResult := TMessageButton(3) else
  if Sender = CancelButton then ModalResult := TMessageButton(4) else
  if Sender = OverwriteAllOlderButton then ModalResult := TMessageButton(5) else
  if Sender = SkipAllButton then ModalResult := TMessageButton(6) else
  if Sender = RenameButton then DoRename else
  if Sender = AppendButton then DoAppend;
end;

procedure TFOverwrite.DoRename;
var AFCopyMove: TFCopyMove;
begin
  AFCopyMove := TFCopyMove.Create(Self);
  try
    AFCopyMove.Entry.Text := RenameStr;
    AFCopyMove.Caption := LANGRename;
    AFCopyMove.Label1.Caption := Format(LANGRenameFile, [RenameStr]);
    AFCopyMove.Entry.SelectRegion(0, Length(AFCopyMove.Entry.Text));
    if AFCopyMove.Run = mbOK then begin
      RenameStr := AFCopyMove.Entry.Text;
      ModalResult := TMessageButton(7);
    end;
  finally
    AFCopyMove.Free;
  end;
end;

procedure TFOverwrite.DoAppend;
begin
  if Application.MessageBox(Format(LANGAppendQuestion, [SourceFile, DestFile]), [mbYes, mbNo], mbQuestion, mbNone, mbNo) = mbYes
    then ModalResult := TMessageButton(8);
end;




end.


