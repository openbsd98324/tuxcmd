(*
    Tux Commander - UPreferences - The Preferences dialog
    Copyright (C) 2006 Tomas Bzatek <tbzatek@users.sourceforge.net>
    Check for updates on tuxcmd.sourceforge.net

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
unit UPreferences;

interface

uses
  glib2, gtk2, pango, SysUtils, Types, Classes, Variants, GTKControls, GTKForms, GTKStdCtrls, GTKExtCtrls, GTKConsts, GTKView,
  GTKUtils, GTKDialogs, GTKPixbuf, GTKClasses, GTKMenus, ULibc,
  UCoreClasses, UGnome;

type
  TFPreferences = class(TGTKDialog)
    TitleFrame, ListFontFrame: TGTKFrame;
    TitleLabel: TGTKLabel;
    TitleEventBox: TGTKEventBox;
    TitleIcon: TGTKImage;
    TitleHBox: TGTKHBox;
    Notebook: TGTKNotebook;
    GeneralPage, ColorsPage, FontsPage, PanelsPage, ApplicationsPage, ExperimentalPage: TGTKVBox;
    RowHeightLabel, NumHistoryItemsLabel, ViewerLabel, EditorLabel, TerminalLabel, GeneralLabel2, ExternalAppsLabel, FontsLabel2,
    ListFontLabel, ListFontPreview, ColorsLabel2, ForegroundLabel, BackgroundLabel, PanelsLabel2, ApplicationsLabel2,
    ExperimentalLabel2, PanelsLabel3, PanelsLabel4: TGTKLabel;
    NormalItemFGColorLabel, ActiveItemFGColorLabel, InactiveItemFGColorLabel, SelectedItemFGColorLabel, LinkItemFGColorLabel,
    DotFileItemFGColorLabel: TGTKLabel;
    ExperimentalWarningLabel: TGTKLabel;
    ExperimentalWarningIcon: TGTKImage;
    ClearROAttr, DisableMouseRename, ShowFiletypeIcons, DefaultFontCheckBox, DefaultRowHeightCheckBox, LynxLikeMotionCheckBox,
    DirsInBoldCheckBox, NewStyleAltOCheckBox, WMCompatModeCheckBox, DisableFileTipsCheckBox, DisableDirectoryBracketsCheckBox,
    InsertMovesDownCheckBox, SpaceMovesDownCheckBox, SelectAllDirectoriesCheckBox, ShowFuncButtonsCheckBox,
    OctalPermissionsCheckBox, FocusRefreshCheckBox, CompatUseLibcSystemCheckBox: TGTKCheckButton;
    NormalItemFGColorDefault, ActiveItemFGColorDefault, InactiveItemFGColorDefault,
    LinkItemFGColorDefault, DotFileItemFGColorDefault: TGTKCheckButton;
    HBox1, HBox2, HBox3, HBox4: TGTKHBox;
    VBox1: TGTKVBox;
    ViewerCombo, EditorCombo, TerminalCombo: TGTKCombo;
    ViewerBrowseButton, EditorBrowseButton, TerminalBrowseButton, ChangeFontButton: TGTKButton;
    NormalItemFGColorButton, ActiveItemFGColorButton, InactiveItemFGColorButton, SelectedItemFGColorButton, LinkItemFGColorButton,
    DotFileItemFGColorButton, NormalItemBGColorButton, ActiveItemBGColorButton, InactiveItemBGColorButton: TGnomeColorButton;
    Table1, Table2, Table3, Table4, Table5, Table6: TGTKTable;
    RowHeightSpinEdit, NumHistoryItemsSpinEdit: TGTKSpinEdit;
    SizeFormatLabel: TGTKLabel;
    SizeFormatOptionMenu: TGTKOptionMenu;
    miSizeFormat1, miSizeFormat2, miSizeFormat3, miSizeFormat4, miSizeFormat5, miSizeFormat6: TGTKMenuItem;
    UseInternalViewerCheckBox: TGTKCheckButton;
    ViewerLabel2, EditorLabel2, TerminalLabel2, CmdLineBehaviourLabel: TGTKLabel;
    ViewerOptionMenu, EditorOptionMenu, TerminalOptionMenu: TGTKOptionMenu;
    miViewerTerminalDetect, miViewerAlwaysTerminal, miViewerNoTerminal,
    miEditorTerminalDetect, miEditorAlwaysTerminal, miEditorNoTerminal,
    miTerminalDetect, miAlwaysTerminal, miNoTerminal: TGTKMenuItem;
    ShowTextUIDsCheckBox: TGTKCheckButton;
    DateFormatLabel: TGTKLabel;
    DateFormatOptionMenu: TGTKOptionMenu;
    CustomDateFormatEntry: TGTKEntry;
    TimeFormatLabel: TGTKLabel;
    TimeFormatOptionMenu: TGTKOptionMenu;
    CustomTimeFormatEntry: TGTKEntry;
    DateTimeFormatLabel: TGTKLabel;
    DateTimeFormatOptionMenu: TGTKOptionMenu;
    QuickSearchLabel: TGTKLabel;
    QuickSearchOptionMenu: TGTKOptionMenu;
    SortDirectoriesLikeFilesCheckBox: TGTKCheckButton;
    TempPathLabel, VFSTempPathLabel: TGTKLabel;
    VFSTempPathEntry: TGTKEntry;
    QuickRenameSkipExtCheckBox: TGTKCheckButton;
    RightClickSelectCheckBox: TGTKCheckButton;

    procedure FormCreate(Sender: TObject); override;
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; Key: Word; Shift: TShiftState; var Accept: boolean);
    procedure ViewerBrowseButtonClick(Sender: TObject);
    procedure DefaultFontCheckBoxToggled(Sender: TObject);
    procedure ChangeFontButtonClick(Sender: TObject);
    procedure ColorButtonDefaultsToggled(Sender: TObject);
    procedure DefaultRowHeightCheckBoxToggled(Sender: TObject);
    procedure UseInternalViewerCheckBoxToggled(Sender: TObject);
    procedure DateTimeFormatOptionMenuChanged(Sender: TObject);
  private
    DefaultGTKFont, LocalListFont: string;
    FormatTime: time_t;
  public
    RebuildListViews, RebuildIcons: boolean;
    procedure AssignDefaultValues;
    procedure SaveSettings;
  end;

var
  FPreferences: TFPreferences;

implementation

uses ULocale, UFileAssoc, UCoreUtils, UConfig;


procedure TFPreferences.FormCreate(Sender: TObject);
var i: integer;
    Item: TGTKMenuItem;
begin
  SetDefaultSize(450, 600);
  Caption := LANGPreferences_Caption;
  Buttons := [mbOK, mbCancel];
  ShowSeparator := False;
  TitleEventBox := TGTKEventBox.Create(Self);
  TitleLabel := TGTKLabel.Create(Self);
  TitleLabel.Caption := LANGPreferences_TitleLabel_Caption;
  TitleLabel.UseMarkup := True;
  TitleLabel.XAlign := 0;
  TitleLabel.XPadding := 0;
  TitleLabel.YPadding := 3;
  TitleEventBox.ControlState := csPrelight;
  TitleFrame := TGTKFrame.CreateWithoutLabel(Self);
  TitleFrame.ShadowType := stShadowOut;
  TitleIcon := TGTKImage.Create(Self);
  TitleIcon.SetFromStock('gtk-preferences', isLargeToolbar);
  TitleHBox := TGTKHBox.Create(Self);
  TitleHBox.Homogeneous := False;
  TitleHBox.AddControlEx(TGTKVBox.Create(Self), False, False, 5);
  TitleHBox.AddControlEx(TitleIcon, False, False, 0);
  TitleHBox.AddControlEx(TitleLabel, True, True, 10);
  TitleEventBox.AddControl(TitleHBox);
  TitleFrame.AddControl(TitleEventBox);
  ClientArea.AddControlEx(TitleFrame, False, True, 0);

  Notebook := TGTKNotebook.Create(Self);
  Notebook.BorderWidth := 10;
  ClientArea.AddControlEx(Notebook, True, True, 0);

  GeneralPage := TGTKVBox.Create(Notebook);
  Notebook.AppendPage(GeneralPage, LANGPreferences_GeneralPage);

  PanelsPage := TGTKVBox.Create(Notebook);
  Notebook.AppendPage(PanelsPage, LANGPreferencesPanelsPage);

  ApplicationsPage := TGTKVBox.Create(Notebook);
  Notebook.AppendPage(ApplicationsPage, LANGPreferencesApplicationsPage);

  FontsPage := TGTKVBox.Create(Notebook);
  Notebook.AppendPage(FontsPage, LANGPreferences_FontsPage);

  ColorsPage := TGTKVBox.Create(Notebook);
  Notebook.AppendPage(ColorsPage, LANGPreferences_ColorsPage);

  ExperimentalPage := TGTKVBox.Create(Notebook);
  Notebook.AppendPage(ExperimentalPage, LANGPreferencesExperimentalPage);


  // *********  PAGE General

  GeneralPage.AddControlEx(TGTKVBox.Create(Self), False, False, 5);
  GeneralLabel2 := TGTKLabel.Create(Self);
  GeneralLabel2.XAlign := 0;
  GeneralLabel2.XPadding := 10;
  GeneralLabel2.Caption := Format('<span weight="ultrabold">%s</span>', [LANGPreferences_GeneralPage]);
  GeneralLabel2.UseMarkup := True;
  GeneralPage.AddControlEx(GeneralLabel2, False, False, 5);
  Table2 := TGTKTable.Create(Self);
  Table2.SetRowColCount(8, 5);
  GeneralPage.AddControlEx(Table2, False, False, 0);

  ClearROAttr := TGTKCheckButton.CreateWithLabel(Self, LANGPreferences_ClearReadonlyAttribute);
  Table2.AddControlEx(1, 0, 3, 1, ClearROAttr, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 0, 2);
  SelectAllDirectoriesCheckBox := TGTKCheckButton.CreateWithLabel(Self, LANGPreferencesSelectAllDirectoriesCheckBox_Caption);
  Table2.AddControlEx(1, 1, 3, 1, SelectAllDirectoriesCheckBox, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 0, 2);
  NewStyleAltOCheckBox := TGTKCheckButton.CreateWithLabel(Self, LANGPreferencesNewStyleAltOCheckBox_Caption);
  NewStyleAltOCheckBox.Tooltip := LANGPreferencesNewStyleAltOCheckBox_Tooltip;
  Table2.AddControlEx(1, 2, 3, 1, NewStyleAltOCheckBox, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 0, 2);
  ShowFuncButtonsCheckBox := TGTKCheckButton.CreateWithLabel(Self, LANGPreferencesShowFuncButtonsCheckBox_Caption);
  Table2.AddControlEx(1, 3, 3, 1, ShowFuncButtonsCheckBox, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 0, 2);

  NumHistoryItemsLabel := TGTKLabel.Create(Self);
  NumHistoryItemsLabel.Caption := LANGPreferences_NumHistoryItems;
  NumHistoryItemsLabel.XAlign := 0;
  NumHistoryItemsSpinEdit := TGTKSpinEdit.Create(Self);
  NumHistoryItemsSpinEdit.Digits := 0;
  NumHistoryItemsLabel.FocusControl := NumHistoryItemsSpinEdit;
  NumHistoryItemsLabel.UseUnderline := True;
  Table2.AddControlEx(0, 4, 1, 1, TGTKVBox.Create(Self), [taoShrink], [taoShrink, taoExpand, taoFill], 15, 2);
  Table2.AddControlEx(1, 4, 1, 1, NumHistoryItemsLabel, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 0, 2);
  Table2.AddControlEx(2, 4, 1, 1, NumHistoryItemsSpinEdit, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 10, 2);

  RowHeightLabel := TGTKLabel.Create(Self);
  RowHeightLabel.Caption := LANGPreferences_RowHeight;
  RowHeightLabel.XAlign := 0;
  RowHeightSpinEdit := TGTKSpinEdit.Create(Self);
  RowHeightSpinEdit.Digits := 0;
  RowHeightLabel.FocusControl := RowHeightSpinEdit;
  RowHeightLabel.UseUnderline := True;
  DefaultRowHeightCheckBox := TGTKCheckButton.CreateWithLabel(Self, LANGPreferences_Default);
  DefaultRowHeightCheckBox.OnToggled := DefaultRowHeightCheckBoxToggled;
  Table2.AddControlEx(1, 5, 1, 1, RowHeightLabel, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 0, 2);
  Table2.AddControlEx(2, 5, 1, 1, RowHeightSpinEdit, [taoFill, taoExpand], [taoShrink, taoExpand, taoFill], 10, 2);
  Table2.AddControlEx(3, 5, 1, 1, DefaultRowHeightCheckBox, [taoFill, taoExpand], [taoShrink, taoExpand, taoFill], 10, 2);
  Table2.AddControlEx(4, 5, 1, 1, TGTKVBox.Create(Self), [taoShrink], [taoShrink, taoExpand, taoFill], 8, 2);

  SizeFormatLabel := TGTKLabel.Create(Self);
  SizeFormatLabel.Caption := LANGPreferencesSizeFormatLabel_Caption;
  SizeFormatLabel.XAlign := 0;
  SizeFormatLabel.UseUnderline := True;
  SizeFormatOptionMenu := TGTKOptionMenu.Create(Self);
  SizeFormatLabel.FocusControl := SizeFormatOptionMenu;
  miSizeFormat1 := TGTKMenuItem.CreateTyped(Self, itLabel);
  miSizeFormat1.Caption := Format('%s (%s)', [LANGPreferencesmiSizeFormat1,  FormatSize(123456, 0, 0)]);
  miSizeFormat2 := TGTKMenuItem.CreateTyped(Self, itLabel);
  miSizeFormat2.Caption := FormatSize(123456, 0, 1);
  miSizeFormat3 := TGTKMenuItem.CreateTyped(Self, itLabel);
  miSizeFormat3.Caption := FormatSize(123456, 0, 2);
  miSizeFormat4 := TGTKMenuItem.CreateTyped(Self, itLabel);
  miSizeFormat4.Caption := FormatSize(123456, 0, 3);
  miSizeFormat5 := TGTKMenuItem.CreateTyped(Self, itLabel);
  miSizeFormat5.Caption := FormatSize(123456, 0, 4);
  miSizeFormat6 := TGTKMenuItem.CreateTyped(Self, itLabel);
  miSizeFormat6.Caption := LANGPreferencesmiSizeFormat6;
  SizeFormatOptionMenu.Items.Add(miSizeFormat1);
  SizeFormatOptionMenu.Items.Add(miSizeFormat2);
  SizeFormatOptionMenu.Items.Add(miSizeFormat3);
  SizeFormatOptionMenu.Items.Add(miSizeFormat4);
  SizeFormatOptionMenu.Items.Add(miSizeFormat5);
  SizeFormatOptionMenu.Items.Add(miSizeFormat6);
  Table2.AddControl(0, 7, 1, 1, TGTKVBox.Create(Self), 0, 4);
  Table2.AddControlEx(1, 8, 1, 1, SizeFormatLabel, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 0, 2);
  Table2.AddControlEx(2, 8, 2, 1, SizeFormatOptionMenu, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 10, 2);

  TerminalOptionMenu := TGTKOptionMenu.Create(Self);
  miTerminalDetect := TGTKMenuItem.CreateTyped(Self, itLabel);
  miTerminalDetect.Caption := LANGPreferencesAutodetectXApp;
  miAlwaysTerminal := TGTKMenuItem.CreateTyped(Self, itLabel);
  miAlwaysTerminal.Caption := LANGPreferencesAlwaysRunInTerminal;
  miNoTerminal := TGTKMenuItem.CreateTyped(Self, itLabel);
  miNoTerminal.Caption := LANGPreferencesNeverRunInTerminal;
  TerminalOptionMenu.Items.Add(miTerminalDetect);
  TerminalOptionMenu.Items.Add(miAlwaysTerminal);
  TerminalOptionMenu.Items.Add(miNoTerminal);
  CmdLineBehaviourLabel := TGTKLabel.Create(Self);
  CmdLineBehaviourLabel.Caption := LANGPreferencesCmdLineBehaviourLabel_Caption;
  CmdLineBehaviourLabel.XAlign := 0;
  CmdLineBehaviourLabel.FocusControl := TerminalOptionMenu;
  CmdLineBehaviourLabel.UseUnderline := True;
  Table2.AddControlEx(1, 6, 1, 1, CmdLineBehaviourLabel, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 0, 2);
  Table2.AddControlEx(2, 6, 2, 1, TerminalOptionMenu, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 10, 2);

  FormatTime := libc_time(nil);
  DateFormatLabel := TGTKLabel.Create(Self);
  DateFormatLabel.Caption := LANGPreferences_DateFormatLabel_Caption;
  DateFormatLabel.XAlign := 0;
  DateFormatLabel.UseUnderline := True;
  DateFormatOptionMenu := TGTKOptionMenu.Create(Self);
  DateFormatLabel.FocusControl := DateFormatOptionMenu;
  Item := TGTKMenuItem.CreateTyped(Self, itLabel);
  Item.Caption := Format('%s (%s)', [LANGPreferences_System, FormatDate(FormatTime, False, True, -1, 0, -1)]);
  DateFormatOptionMenu.Items.Add(Item);
  for i := 1 to 14 do begin
    Item := TGTKMenuItem.CreateTyped(Self, itLabel);
    Item.Caption := FormatDate(FormatTime, False, True, -1, i, -1);
    DateFormatOptionMenu.Items.Add(Item);
  end;
  Item := TGTKMenuItem.CreateTyped(Self, itLabel);
  Item.Caption := LANGPreferences_Custom;
  DateFormatOptionMenu.Items.Add(Item);
  DateFormatOptionMenu.OnChanged := DateTimeFormatOptionMenuChanged;
  Table2.AddControlEx(1, 9, 1, 1, DateFormatLabel, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 0, 2);
  Table2.AddControlEx(2, 9, 2, 1, DateFormatOptionMenu, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 10, 2);

  CustomDateFormatEntry := TGTKEntry.Create(Self);
  CustomDateFormatEntry.Enabled := False;
  CustomDateFormatEntry.Tooltip := LANGPreferences_CustomDateFormatEntry_Tooltip;
  CustomDateFormatEntry.OnChanged := DateTimeFormatOptionMenuChanged;
  Table2.AddControlEx(2, 10, 2, 1, CustomDateFormatEntry, [taoShrink], [taoShrink], 10, 2);

  TimeFormatLabel := TGTKLabel.Create(Self);
  TimeFormatLabel.Caption := LANGPreferences_TimeFormatLabel_Caption;
  TimeFormatLabel.XAlign := 0;
  TimeFormatLabel.UseUnderline := True;
  TimeFormatOptionMenu := TGTKOptionMenu.Create(Self);
  TimeFormatLabel.FocusControl := TimeFormatOptionMenu;
  Item := TGTKMenuItem.CreateTyped(Self, itLabel);
  Item.Caption := Format('%s (%s)', [LANGPreferences_System, FormatDate(FormatTime, True, False, 0, -1, -1)]);
  TimeFormatOptionMenu.Items.Add(Item);
  for i := 1 to 11 do begin
    Item := TGTKMenuItem.CreateTyped(Self, itLabel);
    Item.Caption := FormatDate(FormatTime, True, False, i, -1, -1);
    TimeFormatOptionMenu.Items.Add(Item);
  end;
  Item := TGTKMenuItem.CreateTyped(Self, itLabel);
  Item.Caption := LANGPreferences_Custom;
  TimeFormatOptionMenu.Items.Add(Item);
  TimeFormatOptionMenu.OnChanged := DateTimeFormatOptionMenuChanged;
  Table2.AddControlEx(1, 11, 1, 1, TimeFormatLabel, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 0, 2);
  Table2.AddControlEx(2, 11, 2, 1, TimeFormatOptionMenu, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 10, 2);

  CustomTimeFormatEntry := TGTKEntry.Create(Self);
  CustomTimeFormatEntry.Enabled := False;
  CustomTimeFormatEntry.Tooltip := LANGPreferences_CustomTimeFormatEntry_Tooltip;
  CustomTimeFormatEntry.OnChanged := DateTimeFormatOptionMenuChanged;
  Table2.AddControlEx(2, 12, 2, 1, CustomTimeFormatEntry, [taoShrink], [taoShrink], 10, 2);

  DateTimeFormatLabel := TGTKLabel.Create(Self);
  DateTimeFormatLabel.Caption := LANGPreferences_DateTimeFormatLabel_Caption;
  DateTimeFormatLabel.XAlign := 0;
  DateTimeFormatLabel.UseUnderline := True;
  DateTimeFormatOptionMenu := TGTKOptionMenu.Create(Self);
  DateTimeFormatLabel.FocusControl := DateTimeFormatOptionMenu;
  Item := TGTKMenuItem.CreateTyped(Self, itLabel);
  Item.Caption := FormatDate(FormatTime, True, True, -1, -1, 0);
  DateTimeFormatOptionMenu.Items.Add(Item);
  Item := TGTKMenuItem.CreateTyped(Self, itLabel);
  Item.Caption := FormatDate(FormatTime, True, True, -1, -1, 1);
  DateTimeFormatOptionMenu.Items.Add(Item);
  Table2.AddControlEx(1, 13, 1, 1, DateTimeFormatLabel, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 0, 2);
  Table2.AddControlEx(2, 13, 2, 1, DateTimeFormatOptionMenu, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 10, 2);


  // *********  PAGE Panels

  PanelsPage.AddControlEx(TGTKVBox.Create(Self), False, False, 5);
  PanelsLabel2 := TGTKLabel.Create(Self);
  PanelsLabel2.XAlign := 0;
  PanelsLabel2.XPadding := 10;
  PanelsLabel2.Caption := Format('<span weight="ultrabold">%s</span>', [LANGPreferencesFeatures]);
  PanelsLabel2.UseMarkup := True;
  PanelsPage.AddControlEx(PanelsLabel2, False, False, 5);

  Table5 := TGTKTable.Create(Self);
  Table5.SetRowColCount(19, 2);
  PanelsPage.AddControlEx(Table5, False, False, 0);

  DisableMouseRename := TGTKCheckButton.CreateWithLabel(Self, LANGPreferences_DisableMouseRenaming);
  DisableMouseRename.Tooltip := LANGPreferencesDisableMouseRename_Tooltip;
  Table5.AddControl(0, 0, 2, 1, DisableMouseRename, 30, 2);
  QuickRenameSkipExtCheckBox := TGTKCheckButton.CreateWithLabel(Self, LANGPreferences_QuickRenameSkipExtCheckBox);
  QuickRenameSkipExtCheckBox.Tooltip := LANGPreferences_QuickRenameSkipExtCheckBox_Tooltip;
  Table5.AddControl(0, 1, 2, 1, QuickRenameSkipExtCheckBox, 30, 2);
  RightClickSelectCheckBox := TGTKCheckButton.CreateWithLabel(Self, LANGPreferences_RightClickSelectCheckBox);
  RightClickSelectCheckBox.Tooltip := LANGPreferences_RightClickSelectCheckBox_Tooltip;
  Table5.AddControl(0, 2, 2, 1, RightClickSelectCheckBox, 30, 2);
  DisableFileTipsCheckBox := TGTKCheckButton.CreateWithLabel(Self, LANGPreferencesDisableFileTipsCheckBox_Caption);
  DisableFileTipsCheckBox.Tooltip := LANGPreferencesDisableFileTipsCheckBox_Tooltip;
  Table5.AddControl(0, 3, 2, 1, DisableFileTipsCheckBox, 30, 2);
  SortDirectoriesLikeFilesCheckBox := TGTKCheckButton.CreateWithLabel(Self, LANGPreferences_SortDirectoriesLikeFilesCheckBox);
  SortDirectoriesLikeFilesCheckBox.Tooltip := LANGPreferences_SortDirectoriesLikeFilesCheckBox_Tooltip;
  Table5.AddControl(0, 4, 2, 1, SortDirectoriesLikeFilesCheckBox, 30, 2);
  Table5.AddControl(0, 5, 2, 1, TGTKVBox.Create(Self), 0, 7);

  PanelsLabel3 := TGTKLabel.Create(Self);
  PanelsLabel3.XAlign := 0;
  PanelsLabel3.XPadding := 0;
  PanelsLabel3.Caption := Format('<span weight="ultrabold">%s</span>', [LANGPreferencesShow]);
  PanelsLabel3.UseMarkup := True;
  Table5.AddControl(0, 6, 2, 1, PanelsLabel3, 10, 2);

  ShowFiletypeIcons := TGTKCheckButton.CreateWithLabel(Self, LANGPreferences_ShowFiletypeIconsInList);
  Table5.AddControl(0, 7, 2, 1, ShowFiletypeIcons, 30, 2);
  DirsInBoldCheckBox := TGTKCheckButton.CreateWithLabel(Self, LANGPreferencesDirsInBoldCheckBox_Caption);
  Table5.AddControl(0, 8, 2, 1, DirsInBoldCheckBox, 30, 2);
  DisableDirectoryBracketsCheckBox := TGTKCheckButton.CreateWithLabel(Self, LANGPreferencesDisableDirectoryBracketsCheckBox_Caption);
  Table5.AddControl(0, 9, 2, 1, DisableDirectoryBracketsCheckBox, 30, 2);
  OctalPermissionsCheckBox := TGTKCheckButton.CreateWithLabel(Self, LANGPreferencesOctalPermissionsCheckBox_Caption);
  OctalPermissionsCheckBox.Tooltip := LANGPreferencesOctalPermissionsCheckBox_Tooltip;
  Table5.AddControl(0, 10, 2, 1, OctalPermissionsCheckBox, 30, 2);
  ShowTextUIDsCheckBox := TGTKCheckButton.CreateWithLabel(Self, LANGShowTextUIDsCheckBox_Caption);
  ShowTextUIDsCheckBox.Tooltip := LANGShowTextUIDsCheckBox_Tooltip;
  Table5.AddControl(0, 11, 2, 1, ShowTextUIDsCheckBox, 30, 2);
  Table5.AddControl(0, 12, 2, 1, TGTKVBox.Create(Self), 0, 7);

  PanelsLabel4 := TGTKLabel.Create(Self);
  PanelsLabel4.XAlign := 0;
  PanelsLabel4.XPadding := 0;
  PanelsLabel4.Caption := Format('<span weight="ultrabold">%s</span>', [LANGPreferencesMovement]);
  PanelsLabel4.UseMarkup := True;
  Table5.AddControl(0, 13, 2, 1, PanelsLabel4, 10, 2);

  LynxLikeMotionCheckBox := TGTKCheckButton.CreateWithLabel(Self, LANGPreferencesLynxLikeMotionCheckBox_Caption);
  Table5.AddControl(0, 14, 2, 1, LynxLikeMotionCheckBox, 30, 2);
  InsertMovesDownCheckBox := TGTKCheckButton.CreateWithLabel(Self, LANGPreferencesInsertMovesDownCheckBox_Caption);
  Table5.AddControl(0, 15, 2, 1, InsertMovesDownCheckBox, 30, 2);
  SpaceMovesDownCheckBox := TGTKCheckButton.CreateWithLabel(Self, LANGPreferencesSpaceMovesDownCheckBox_Caption);
  Table5.AddControl(0, 16, 2, 1, SpaceMovesDownCheckBox, 30, 2);

  QuickSearchLabel := TGTKLabel.Create(Self);
  QuickSearchLabel.Caption := LANGPreferences_QuickSearchLabel_Caption;
  QuickSearchLabel.XAlign := 0;
  QuickSearchLabel.UseUnderline := True;
  QuickSearchOptionMenu := TGTKOptionMenu.Create(Self);
  QuickSearchOptionMenu.Tooltip := LANGPreferences_QuickSearchOptionMenu_Tooltip;
  QuickSearchLabel.FocusControl := QuickSearchOptionMenu;
  Item := TGTKMenuItem.CreateTyped(Self, itLabel);
  Item.Caption := LANGPreferences_QuickSearch_Option1;
  QuickSearchOptionMenu.Items.Add(Item);
  Item := TGTKMenuItem.CreateTyped(Self, itLabel);
  Item.Caption := LANGPreferences_QuickSearch_Option2;
  QuickSearchOptionMenu.Items.Add(Item);
  Item := TGTKMenuItem.CreateTyped(Self, itLabel);
  Item.Caption := LANGPreferences_QuickSearch_Option3;
  QuickSearchOptionMenu.Items.Add(Item);
  Item := TGTKMenuItem.CreateTyped(Self, itLabel);
  Item.Caption := LANGPreferences_QuickSearch_Option4;
  QuickSearchOptionMenu.Items.Add(Item);
  Table5.AddControl(0, 17, 2, 1, TGTKVBox.Create(Self), 0, 2);
  Table5.AddControlEx(0, 18, 1, 1, QuickSearchLabel, [taoShrink], [taoShrink], 35, 2);
  Table5.AddControlEx(1, 18, 1, 1, QuickSearchOptionMenu, [taoExpand, taoFill], [taoShrink, taoExpand, taoFill], 20, 2);


  // *********  PAGE Applications

  ApplicationsPage.AddControlEx(TGTKVBox.Create(Self), False, False, 5);
  Table1 := TGTKTable.Create(Self);
  Table1.SetRowColCount(5, 5);
  ApplicationsPage.AddControlEx(Table1, False, True, 5);

  ViewerLabel := TGTKLabel.Create(Self);
  ViewerLabel.XAlign := 0;
  ViewerLabel.XPadding := 10;
  ViewerLabel.Caption := Format('<span weight="ultrabold">%s</span>', [LANGPreferencesViewer]);
  ViewerLabel.UseMarkup := True;
  Table1.AddControlEx(0, 0, 5, 1, ViewerLabel, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 0, 0);
  Table1.AddControlEx(0, 1, 5, 1, TGTKVBox.Create(Self), [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 0, 2);

  ViewerCombo := TGTKCombo.Create(Self);
  for i := 1 to Length(ConfViewersApps) do
    ViewerCombo.Items.Append(ConfViewersApps[i]);
  ViewerBrowseButton := TGTKButton.Create(Self);
  ViewerBrowseButton.Caption := Format('  %s  ', [LANGBrowseButton_Caption]);
  ViewerBrowseButton.OnClick := ViewerBrowseButtonClick;
  ViewerLabel2 := TGTKLabel.Create(Self);
  ViewerLabel2.Caption := LANGPreferencesCommandSC;
  ViewerLabel2.XAlign := 0;
  ViewerLabel2.FocusControl := ViewerCombo.Entry;
  ViewerLabel2.UseUnderline := True;
  UseInternalViewerCheckBox := TGTKCheckButton.CreateWithLabel(Self, LANGPreferencesUseInternalViewer);
  UseInternalViewerCheckBox.OnToggled := UseInternalViewerCheckBoxToggled;
  ViewerOptionMenu := TGTKOptionMenu.Create(Self);
  miViewerTerminalDetect := TGTKMenuItem.CreateTyped(Self, itLabel);
  miViewerTerminalDetect.Caption := LANGPreferencesAutodetectXApp;
  miViewerAlwaysTerminal := TGTKMenuItem.CreateTyped(Self, itLabel);
  miViewerAlwaysTerminal.Caption := LANGPreferencesAlwaysRunInTerminal;
  miViewerNoTerminal := TGTKMenuItem.CreateTyped(Self, itLabel);
  miViewerNoTerminal.Caption := LANGPreferencesNeverRunInTerminal;
  ViewerOptionMenu.Items.Add(miViewerTerminalDetect);
  ViewerOptionMenu.Items.Add(miViewerAlwaysTerminal);
  ViewerOptionMenu.Items.Add(miViewerNoTerminal);

  Table1.AddControlEx(0, 2, 1, 1, TGTKVBox.Create(Self), [taoShrink], [taoShrink, taoExpand, taoFill], 12, 2);
  Table1.AddControlEx(1, 2, 1, 1, ViewerLabel2, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 0, 2);
  Table1.AddControlEx(2, 2, 3, 1, ViewerCombo, [taoExpand, taoFill], [taoShrink, taoExpand, taoFill], 10, 2);
  Table1.AddControlEx(4, 3, 1, 1, ViewerBrowseButton, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 10, 2);
  Table1.AddControlEx(3, 3, 1, 1, ViewerOptionMenu, [taoExpand, taoFill], [taoShrink, taoExpand, taoFill], 10, 2);
  Table1.AddControlEx(0, 4, 5, 1, UseInternalViewerCheckBox, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 30, 2);

  EditorLabel := TGTKLabel.Create(Self);
  EditorLabel.XAlign := 0;
  EditorLabel.XPadding := 10;
  EditorLabel.Caption := Format('<span weight="ultrabold">%s</span>', [LANGPreferencesEditor]);
  EditorLabel.UseMarkup := True;
  Table1.AddControlEx(0, 5, 5, 1, TGTKVBox.Create(Self), [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 0, 13);
  Table1.AddControlEx(0, 6, 5, 1, EditorLabel, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 0, 0);
  Table1.AddControlEx(0, 7, 5, 1, TGTKVBox.Create(Self), [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 0, 2);

  EditorCombo := TGTKCombo.Create(Self);
  for i := 1 to Length(ConfEditorApps) do
    EditorCombo.Items.Append(ConfEditorApps[i]);
  EditorBrowseButton := TGTKButton.Create(Self);
  EditorBrowseButton.Caption := Format('  %s  ', [LANGBrowseButton_Caption]);
  EditorBrowseButton.OnClick := ViewerBrowseButtonClick;
  EditorLabel2 := TGTKLabel.Create(Self);
  EditorLabel2.Caption := LANGPreferencesCommandSC;
  EditorLabel2.XAlign := 0;
  EditorLabel2.FocusControl := EditorCombo.Entry;
  EditorLabel2.UseUnderline := True;
  EditorOptionMenu := TGTKOptionMenu.Create(Self);
  miEditorTerminalDetect := TGTKMenuItem.CreateTyped(Self, itLabel);
  miEditorTerminalDetect.Caption := LANGPreferencesAutodetectXApp;
  miEditorAlwaysTerminal := TGTKMenuItem.CreateTyped(Self, itLabel);
  miEditorAlwaysTerminal.Caption := LANGPreferencesAlwaysRunInTerminal;
  miEditorNoTerminal := TGTKMenuItem.CreateTyped(Self, itLabel);
  miEditorNoTerminal.Caption := LANGPreferencesNeverRunInTerminal;
  EditorOptionMenu.Items.Add(miEditorTerminalDetect);
  EditorOptionMenu.Items.Add(miEditorAlwaysTerminal);
  EditorOptionMenu.Items.Add(miEditorNoTerminal);

  Table1.AddControlEx(0, 8, 1, 1, TGTKVBox.Create(Self), [taoShrink], [taoShrink, taoExpand, taoFill], 12, 2);
  Table1.AddControlEx(1, 8, 1, 1, EditorLabel2, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 0, 2);
  Table1.AddControlEx(2, 8, 3, 1, EditorCombo, [taoExpand, taoFill], [taoShrink, taoExpand, taoFill], 10, 2);
  Table1.AddControlEx(4, 9, 1, 1, EditorBrowseButton, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 10, 2);
  Table1.AddControlEx(3, 9, 1, 1, EditorOptionMenu, [taoExpand, taoFill], [taoShrink, taoExpand, taoFill], 10, 2);

  TerminalLabel := TGTKLabel.Create(Self);
  TerminalLabel.XAlign := 0;
  TerminalLabel.XPadding := 10;
  TerminalLabel.Caption := Format('<span weight="ultrabold">%s</span>', [LANGPreferencesTerminal]);
  TerminalLabel.UseMarkup := True;
  Table1.AddControlEx(0, 10, 5, 1, TGTKVBox.Create(Self), [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 0, 13);
  Table1.AddControlEx(0, 11, 5, 1, TerminalLabel, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 0, 0);
  Table1.AddControlEx(0, 12, 5, 1, TGTKVBox.Create(Self), [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 0, 2);

  TerminalCombo := TGTKCombo.Create(Self);
  for i := 1 to Length(ConfTerminalApps) do
    TerminalCombo.Items.Append(ConfTerminalApps[i]);
  TerminalBrowseButton := TGTKButton.Create(Self);
  TerminalBrowseButton.Caption := Format('  %s  ', [LANGBrowseButton_Caption]);
  TerminalBrowseButton.OnClick := ViewerBrowseButtonClick;
  TerminalLabel2 := TGTKLabel.Create(Self);
  TerminalLabel2.Caption := LANGPreferencesCommandSC;
  TerminalLabel2.XAlign := 0;
  TerminalLabel2.FocusControl := TerminalCombo.Entry;
  TerminalLabel2.UseUnderline := True;

  Table1.AddControlEx(0, 13, 1, 1, TGTKVBox.Create(Self), [taoShrink], [taoShrink, taoExpand, taoFill], 12, 2);
  Table1.AddControlEx(1, 13, 1, 1, TerminalLabel2, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 0, 2);
  Table1.AddControlEx(2, 13, 3, 1, TerminalCombo, [taoExpand, taoFill], [taoShrink, taoExpand, taoFill], 10, 2);
  Table1.AddControlEx(4, 14, 1, 1, TerminalBrowseButton, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 10, 2);

  TempPathLabel := TGTKLabel.Create(Self);
  TempPathLabel.XAlign := 0;
  TempPathLabel.XPadding := 10;
  TempPathLabel.Caption := Format('<span weight="ultrabold">%s</span>', [LANGPreferences_TempPathLabel_Caption]);
  TempPathLabel.UseMarkup := True;
  Table1.AddControlEx(0, 15, 5, 1, TGTKVBox.Create(Self), [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 0, 13);
  Table1.AddControlEx(0, 16, 5, 1, TempPathLabel, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 0, 0);
  Table1.AddControlEx(0, 17, 5, 1, TGTKVBox.Create(Self), [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 0, 2);

  VFSTempPathLabel := TGTKLabel.Create(Self);
  VFSTempPathLabel.Caption := LANGPreferences_VFSTempPathLabel_Caption;
  VFSTempPathLabel.XAlign := 0;
  VFSTempPathLabel.UseUnderline := True;
  VFSTempPathEntry := TGTKEntry.Create(Self);
  VFSTempPathEntry.Tooltip := LANGPreferences_VFSTempPathEntry_Tooltip;
  VFSTempPathLabel.FocusControl := VFSTempPathEntry;
  Table1.AddControlEx(0, 18, 1, 1, TGTKVBox.Create(Self), [taoShrink], [taoShrink, taoExpand, taoFill], 12, 2);
  Table1.AddControlEx(1, 18, 1, 1, VFSTempPathLabel, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 0, 2);
  Table1.AddControlEx(2, 18, 4, 1, VFSTempPathEntry, [taoExpand, taoFill], [taoShrink, taoExpand, taoFill], 10, 2);


  // *********  PAGE Fonts

  FontsPage.AddControlEx(TGTKVBox.Create(Self), False, False, 5);
  FontsLabel2 := TGTKLabel.Create(Self);
  FontsLabel2.XAlign := 0;
  FontsLabel2.XPadding := 10;
  FontsLabel2.Caption := Format('<span weight="ultrabold">%s</span>', [LANGPreferences_FontsPage]);
  FontsLabel2.UseMarkup := True;
  FontsPage.AddControlEx(FontsLabel2, False, False, 5);

  Table3 := TGTKTable.Create(Self);
  Table3.SetRowColCount(2, 4);
  FontsPage.AddControlEx(Table3, False, False, 0);
  ListFontLabel := TGTKLabel.Create(Self);
  ListFontLabel.Caption := LANGPreferences_ListFont;
  ListFontLabel.XAlign := 0;
  ListFontFrame := TGTKFrame.CreateWithoutLabel(Self);
  ListFontFrame.ShadowType := stEtchedIn;
  ListFontPreview := TGTKLabel.Create(Self);
  ListFontPreview.Caption := 'Luxi Sans, 10';
  ListFontFrame.AddControl(ListFontPreview);
  ChangeFontButton := TGTKButton.Create(Self);
  ChangeFontButton.Caption := LANGPreferences_Change;
  ChangeFontButton.OnClick := ChangeFontButtonClick;
  DefaultFontCheckBox := TGTKCheckButton.CreateWithLabel(Self, LANGPreferences_UseDefaultFont);
  DefaultFontCheckBox.OnToggled := DefaultFontCheckBoxToggled;

  Table3.AddControlEx(0, 0, 1, 1, TGTKVBox.Create(Self), [taoShrink], [taoShrink, taoExpand, taoFill], 12, 2);
  Table3.AddControlEx(1, 0, 1, 1, ListFontLabel, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 5, 2);
  Table3.AddControlEx(2, 0, 1, 1, ListFontFrame, [taoExpand, taoFill], [taoShrink, taoExpand, taoFill], 2, 2);
  Table3.AddControlEx(3, 0, 1, 1, ChangeFontButton, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 10, 2);
  Table3.AddControlEx(1, 1, 3, 1, DefaultFontCheckBox, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 10, 4);


  // *********  PAGE Colors

  ColorsPage.AddControlEx(TGTKVBox.Create(Self), False, False, 5);
  ColorsLabel2 := TGTKLabel.Create(Self);
  ColorsLabel2.XAlign := 0;
  ColorsLabel2.XPadding := 10;
  ColorsLabel2.Caption := Format('<span weight="ultrabold">%s</span>', [LANGPreferences_ColorsPage]);
  ColorsLabel2.UseMarkup := True;
  ColorsPage.AddControlEx(ColorsLabel2, False, False, 5);

  Table4 := TGTKTable.Create(Self);
  Table4.SetRowColCount(7, 8);
  ColorsPage.AddControlEx(Table4, False, False, 0);
  ForegroundLabel := TGTKLabel.Create(Self);
  ForegroundLabel.Caption := LANGPreferences_Foreground;
  BackgroundLabel := TGTKLabel.Create(Self);
  BackgroundLabel.Caption := LANGPreferences_Background;
  Table4.AddControl(1, 0, 3, 1, ForegroundLabel, 5, 2);
  Table4.AddControl(4, 0, 3, 1, BackgroundLabel, 5, 2);

  NormalItemFGColorLabel := TGTKLabel.Create(Self);
  NormalItemFGColorLabel.XAlign := 0;
  NormalItemFGColorLabel.Caption := LANGPreferences_NormalItem;
  NormalItemFGColorButton := TGnomeColorButton.Create(Self);
  NormalItemFGColorButton.SetSizeRequest(30, -1);
  NormalItemBGColorButton := TGnomeColorButton.Create(Self);
  NormalItemBGColorButton.SetSizeRequest(30, -1);
  NormalItemFGColorDefault := TGTKCheckButton.CreateWithLabel(Self, LANGPreferences_Default);
  NormalItemFGColorDefault.Tooltip := LANGPreferences_SetToDefaultToUseGTKThemeColors;
  NormalItemFGColorDefault.OnToggled := ColorButtonDefaultsToggled;
  Table4.AddControl(0, 1, 1, 1, NormalItemFGColorLabel, 30, 2);
  Table4.AddControl(2, 1, 1, 1, NormalItemFGColorButton, 5, 2);
  Table4.AddControl(5, 1, 1, 1, NormalItemBGColorButton, 5, 2);
  Table4.AddControl(7, 1, 1, 1, NormalItemFGColorDefault, 10, 2);

  ActiveItemFGColorLabel := TGTKLabel.Create(Self);
  ActiveItemFGColorLabel.Caption := LANGPreferences_Cursor;
  ActiveItemFGColorLabel.XAlign := 0;
  ActiveItemFGColorButton := TGnomeColorButton.Create(Self);
  ActiveItemBGColorButton := TGnomeColorButton.Create(Self);
  ActiveItemFGColorDefault := TGTKCheckButton.CreateWithLabel(Self, LANGPreferences_Default);
  ActiveItemFGColorDefault.Tooltip := LANGPreferences_SetToDefaultToUseGTKThemeColors;
  ActiveItemFGColorDefault.OnToggled := ColorButtonDefaultsToggled;
  Table4.AddControl(0, 2, 1, 1, ActiveItemFGColorLabel, 30, 2);
  Table4.AddControl(2, 2, 1, 1, ActiveItemFGColorButton, 5, 2);
  Table4.AddControl(5, 2, 1, 1, ActiveItemBGColorButton, 5, 2);
  Table4.AddControl(7, 2, 1, 1, ActiveItemFGColorDefault, 10, 2);

  InactiveItemFGColorLabel := TGTKLabel.Create(Self);
  InactiveItemFGColorLabel.Caption := LANGPreferences_InactiveItem;
  InactiveItemFGColorLabel.XAlign := 0;
  InactiveItemFGColorButton := TGnomeColorButton.Create(Self);
  InactiveItemBGColorButton := TGnomeColorButton.Create(Self);
  InactiveItemFGColorDefault := TGTKCheckButton.CreateWithLabel(Self, LANGPreferences_Default);
  InactiveItemFGColorDefault.Tooltip := LANGPreferences_SetToDefaultToUseGTKThemeColors;
  InactiveItemFGColorDefault.OnToggled := ColorButtonDefaultsToggled;
  Table4.AddControl(0, 3, 1, 1, InactiveItemFGColorLabel, 30, 2);
  Table4.AddControl(2, 3, 1, 1, InactiveItemFGColorButton, 5, 2);
  Table4.AddControl(5, 3, 1, 1, InactiveItemBGColorButton, 5, 2);
  Table4.AddControl(7, 3, 1, 1, InactiveItemFGColorDefault, 10, 2);

  SelectedItemFGColorLabel := TGTKLabel.Create(Self);
  SelectedItemFGColorLabel.Caption := LANGPreferences_SelectedItem;
  SelectedItemFGColorLabel.XAlign := 0;
  SelectedItemFGColorButton := TGnomeColorButton.Create(Self);
  Table4.AddControl(0, 4, 1, 1, SelectedItemFGColorLabel, 30, 2);
  Table4.AddControl(2, 4, 1, 1, SelectedItemFGColorButton, 5, 2);

  LinkItemFGColorLabel := TGTKLabel.Create(Self);
  LinkItemFGColorLabel.Caption := LANGPreferences_LinkItem;
  LinkItemFGColorLabel.XAlign := 0;
  LinkItemFGColorButton := TGnomeColorButton.Create(Self);
  LinkItemFGColorDefault := TGTKCheckButton.CreateWithLabel(Self, LANGPreferences_Default);
  LinkItemFGColorDefault.Tooltip := LANGPreferences_LinkItemHint;
  LinkItemFGColorDefault.OnToggled := ColorButtonDefaultsToggled;
  Table4.AddControl(0, 5, 1, 1, LinkItemFGColorLabel, 30, 2);
  Table4.AddControl(2, 5, 1, 1, LinkItemFGColorButton, 5, 2);
  Table4.AddControl(7, 5, 1, 1, LinkItemFGColorDefault, 10, 2);

  DotFileItemFGColorLabel := TGTKLabel.Create(Self);
  DotFileItemFGColorLabel.Caption := LANGPreferences_DotFileItem;
  DotFileItemFGColorLabel.XAlign := 0;
  DotFileItemFGColorButton := TGnomeColorButton.Create(Self);
  DotFileItemFGColorDefault := TGTKCheckButton.CreateWithLabel(Self, LANGPreferences_Default);
  DotFileItemFGColorDefault.Tooltip := LANGPreferences_DotFileItemHint;
  DotFileItemFGColorDefault.OnToggled := ColorButtonDefaultsToggled;
  Table4.AddControl(0, 6, 1, 1, DotFileItemFGColorLabel, 30, 2);
  Table4.AddControl(2, 6, 1, 1, DotFileItemFGColorButton, 5, 2);
  Table4.AddControl(7, 6, 1, 1, DotFileItemFGColorDefault, 10, 2);

  Table4.AddControl(1, 6, 1, 1, TGTKVBox.Create(Self), 2, 2);
  Table4.AddControl(3, 6, 1, 1, TGTKVBox.Create(Self), 2, 2);
  Table4.AddControl(4, 6, 1, 1, TGTKVBox.Create(Self), 2, 2);
  Table4.AddControl(6, 6, 1, 1, TGTKVBox.Create(Self), 2, 2);

  
  // *********  PAGE Experimental

  ExperimentalPage.AddControlEx(TGTKVBox.Create(Self), False, False, 5);
  ExperimentalLabel2 := TGTKLabel.Create(Self);
  ExperimentalLabel2.XAlign := 0;
  ExperimentalLabel2.XPadding := 10;
  ExperimentalLabel2.Caption := Format('<span weight="ultrabold">%s</span>', [LANGPreferencesExperimentalFeatures]);
  ExperimentalLabel2.UseMarkup := True;
  ExperimentalPage.AddControlEx(ExperimentalLabel2, False, False, 5);

  HBox4 := TGTKHBox.Create(Self);
  HBox4.Homogeneous := False;
  ExperimentalWarningLabel := TGTKLabel.Create(Self);
  ExperimentalWarningLabel.Caption := LANGPreferencesExperimentalWarningLabel_Caption;
  ExperimentalWarningLabel.UseMarkup := True;
  ExperimentalWarningLabel.LineWrap := True;
  ExperimentalWarningLabel.SetSizeRequest(300, -1);
  ExperimentalWarningIcon := TGTKImage.Create(Self);
  ExperimentalWarningIcon.SetFromStock('gtk-dialog-warning', isDialog);
  HBox4.AddControlEx(TGTKVBox.Create(Self), False, False, 12);
  HBox4.AddControlEx(ExperimentalWarningIcon, False, False, 7);
  HBox4.AddControlEx(ExperimentalWarningLabel, True, True, 7);
  HBox4.AddControlEx(TGTKVBox.Create(Self), False, False, 8);
  ExperimentalPage.AddControlEx(HBox4, False, False, 5);
  ExperimentalPage.AddControlEx(TGTKVBox.Create(Self), False, False, 10);

  Table6 := TGTKTable.Create(Self);
  Table6.SetRowColCount(3, 1);
  ExperimentalPage.AddControlEx(Table6, False, False, 0);

  FocusRefreshCheckBox := TGTKCheckButton.CreateWithLabel(Self, LANGPreferencesFocusRefreshCheckBox_Caption);
  FocusRefreshCheckBox.Tooltip := LANGPreferencesFocusRefreshCheckBox_Tooltip;
  Table6.AddControl(0, 0, 1, 1, FocusRefreshCheckBox, 30, 2);
  WMCompatModeCheckBox := TGTKCheckButton.CreateWithLabel(Self, LANGPreferencesWMCompatModeCheckBox_Caption);
  WMCompatModeCheckBox.Tooltip := LANGPreferencesWMCompatModeCheckBox_Tooltip;
  Table6.AddControl(0, 1, 1, 1, WMCompatModeCheckBox, 30, 2);
  CompatUseLibcSystemCheckBox := TGTKCheckButton.CreateWithLabel(Self, LANGPreferencesCompatUseLibcSystemCheckBox_Caption);
  CompatUseLibcSystemCheckBox.Tooltip := LANGPreferencesCompatUseLibcSystemCheckBox_Tooltip;
  Table6.AddControl(0, 2, 1, 1, CompatUseLibcSystemCheckBox, 30, 2);

  // ******************

  OnKeyDown := FormKeyDown;
  OnDestroy := FormDestroy;
end;

procedure TFPreferences.FormKeyDown(Sender: TObject; Key: Word; Shift: TShiftState; var Accept: boolean);
begin
//  if Key = GDK_ESCAPE then ModalResult := mbCancel;
end;

procedure TFPreferences.FormDestroy(Sender: TObject);
begin

end;

procedure TFPreferences.DateTimeFormatOptionMenuChanged(Sender: TObject);
var Item: TGTKMenuItem;
    OldIndex: integer;
begin
  CustomTimeFormatEntry.Enabled := TimeFormatOptionMenu.ItemIndex = TimeFormatOptionMenu.Items.Count - 1;
  CustomDateFormatEntry.Enabled := DateFormatOptionMenu.ItemIndex = DateFormatOptionMenu.Items.Count - 1;
  
  OldIndex := DateTimeFormatOptionMenu.ItemIndex;
  DateTimeFormatOptionMenu.Items.Clear;
  Item := TGTKMenuItem.CreateTyped(Self, itLabel);
  Item.Caption := FormatDate(FormatTime, True, True, TimeFormatOptionMenu.ItemIndex, DateFormatOptionMenu.ItemIndex, 0, CustomDateFormatEntry.Text, CustomTimeFormatEntry.Text);
  DateTimeFormatOptionMenu.Items.Add(Item);
  Item := TGTKMenuItem.CreateTyped(Self, itLabel);
  Item.Caption := FormatDate(FormatTime, True, True, TimeFormatOptionMenu.ItemIndex, DateFormatOptionMenu.ItemIndex, 1, CustomDateFormatEntry.Text, CustomTimeFormatEntry.Text);
  DateTimeFormatOptionMenu.Items.Add(Item);
  DateTimeFormatOptionMenu.ItemIndex := OldIndex;
end;

(********************************************************************************************************************************)
procedure TFPreferences.ViewerBrowseButtonClick(Sender: TObject);

  procedure BrowseForFile(ToControl: TGTKEntry);
  var Dialog: TGTKFileSelectionDialog;
  begin
    Dialog := TGTKFileSelectionDialog.CreateWithTitle(Self, LANGPreferences_BrowseForApplication);
    try
      Dialog.ShowFileOpButtons := False;
      Dialog.FileName := ToControl.Text;
      if Byte(Dialog.Run) = 251 then ToControl.Text := Dialog.FileName;
    finally
      Dialog.Free;
    end;
  end;

begin
  if Sender = ViewerBrowseButton then BrowseForFile(ViewerCombo.Entry)
  else if Sender = EditorBrowseButton then BrowseForFile(EditorCombo.Entry)
  else if Sender = TerminalBrowseButton then BrowseForFile(TerminalCombo.Entry);
end;

procedure TFPreferences.DefaultFontCheckBoxToggled(Sender: TObject);
var FontDesc: PPangoFontDescription;
begin
  ListFontLabel.Enabled := not DefaultFontCheckBox.Checked;
  ChangeFontButton.Enabled := not DefaultFontCheckBox.Checked;
  ListFontFrame.Enabled := not DefaultFontCheckBox.Checked;
  if DefaultFontCheckBox.Checked then begin
    gtk_widget_modify_font(ListFontPreview.FWidget, nil);
    ListFontPreview.Caption := Format(LANGPreferences_DefaultS, [DefaultGTKFont]);
  end else begin
    FontDesc := pango_font_description_from_string(StringToPgchar(LocalListFont));
    gtk_widget_modify_font(ListFontPreview.FWidget, FontDesc);
    ListFontPreview.Caption := LocalListFont;
  end;
end;

procedure TFPreferences.ChangeFontButtonClick(Sender: TObject);

  procedure ChangeFont(ToControl: TGTKLabel);
  var Dialog: TGTKFontSelectionDialog;
      FontDesc: PPangoFontDescription;
  begin
    Dialog := TGTKFontSelectionDialog.CreateWithTitle(Self, LANGPreferences_SelectFont);
    try
      Dialog.FontName := ToControl.Caption;
      if Byte(Dialog.Run) = 251 then begin
        ToControl.Caption := Dialog.FontName;
        FontDesc := pango_font_description_from_string(StringToPgchar(Dialog.FontName));
        gtk_widget_modify_font(ToControl.FWidget, FontDesc);
//        g_object_unref(FontDesc);   // *** - cannot unref, causes SIGSEGV
        LocalListFont := Dialog.FontName;
      end;
    finally
      Dialog.Free;
    end;
  end;

begin
  if Sender = ChangeFontButton then ChangeFont(ListFontPreview);
end;

procedure TFPreferences.ColorButtonDefaultsToggled(Sender: TObject);
begin
  if NormalItemFGColorDefault.Checked then NormalItemFGColorButton.SetDefaultColor
                                      else NormalItemFGColorButton.UnsetDefaultColor;
  if NormalItemFGColorDefault.Checked then NormalItemBGColorButton.SetDefaultColor
                                      else NormalItemBGColorButton.UnsetDefaultColor;
  if ActiveItemFGColorDefault.Checked then ActiveItemFGColorButton.SetDefaultColor
                                      else ActiveItemFGColorButton.UnsetDefaultColor;
  if ActiveItemFGColorDefault.Checked then ActiveItemBGColorButton.SetDefaultColor
                                      else ActiveItemBGColorButton.UnsetDefaultColor;
  if InactiveItemFGColorDefault.Checked then InactiveItemFGColorButton.SetDefaultColor
                                        else InactiveItemFGColorButton.UnsetDefaultColor;
  if InactiveItemFGColorDefault.Checked then InactiveItemBGColorButton.SetDefaultColor
                                        else InactiveItemBGColorButton.UnsetDefaultColor;
  if LinkItemFGColorDefault.Checked then LinkItemFGColorButton.SetDefaultColor
                                    else LinkItemFGColorButton.UnsetDefaultColor;
  if DotFileItemFGColorDefault.Checked then DotFileItemFGColorButton.SetDefaultColor
                                       else DotFileItemFGColorButton.UnsetDefaultColor;
end;

procedure TFPreferences.DefaultRowHeightCheckBoxToggled(Sender: TObject);
begin
  RowHeightLabel.Enabled := not DefaultRowHeightCheckBox.Checked;
  RowHeightSpinEdit.Enabled := not DefaultRowHeightCheckBox.Checked;
end;

procedure TFPreferences.UseInternalViewerCheckBoxToggled(Sender: TObject);
begin
  ViewerCombo.Enabled := not UseInternalViewerCheckBox.Checked;
  ViewerLabel2.Enabled := not UseInternalViewerCheckBox.Checked;
  ViewerBrowseButton.Enabled := not UseInternalViewerCheckBox.Checked;
  ViewerOptionMenu.Enabled := not UseInternalViewerCheckBox.Checked;
end;

(********************************************************************************************************************************)
procedure TFPreferences.AssignDefaultValues;
var AColor: TGDKColor;
begin
  DefaultGTKFont := pango_font_description_to_string(pango_context_get_font_description(gtk_widget_get_pango_context(FWidget)));
  LocalListFont := ConfPanelFont;

  RowHeightSpinEdit.Min := 6;
  RowHeightSpinEdit.Max := 128;
  RowHeightSpinEdit.AsInteger := ConfRowHeightReal;
  DefaultRowHeightCheckBox.Checked := ConfRowHeight < 0;
  NumHistoryItemsSpinEdit.Min := 1;
  NumHistoryItemsSpinEdit.Max := 512;
  NumHistoryItemsSpinEdit.AsInteger := ConfNumHistoryItems;
  ClearROAttr.Checked := ConfClearReadOnlyAttr;
  DisableMouseRename.Checked := ConfDisableMouseRename;
  ShowFiletypeIcons.Checked := ConfUseFileTypeIcons;
  ViewerCombo.Entry.Text := StrToUTF8(ConfViewer);
  EditorCombo.Entry.Text := StrToUTF8(ConfEditor);
  TerminalCombo.Entry.Text := StrToUTF8(ConfTerminalCommand);
  DefaultFontCheckBox.Checked := ConfUseSystemFont;

  if not StringToGDKColor(ConfNormalItemFGColor, AColor) then StringToGDKColor(ConfDefaultNormalItemFGColor, AColor);
  NormalItemFGColorButton.Color := AColor;
  if not StringToGDKColor(ConfNormalItemBGColor, AColor) then StringToGDKColor(ConfDefaultNormalItemBGColor, AColor);
  NormalItemBGColorButton.Color := AColor;
  if not StringToGDKColor(ConfActiveItemFGColor, AColor) then StringToGDKColor(ConfDefaultActiveItemFGColor, AColor);
  ActiveItemFGColorButton.Color := AColor;
  if not StringToGDKColor(ConfActiveItemBGColor, AColor) then StringToGDKColor(ConfDefaultActiveItemBGColor, AColor);
  ActiveItemBGColorButton.Color := AColor;
  if not StringToGDKColor(ConfInactiveItemFGColor, AColor) then StringToGDKColor(ConfDefaultInactiveItemFGColor, AColor);
  InactiveItemFGColorButton.Color := AColor;
  if not StringToGDKColor(ConfInactiveItemBGColor, AColor) then StringToGDKColor(ConfDefaultInactiveItemBGColor, AColor);
  InactiveItemBGColorButton.Color := AColor;
  if not StringToGDKColor(ConfSelectedItemFGColor, AColor) then StringToGDKColor(ConfDefaultSelectedItemFGColor, AColor);
  SelectedItemFGColorButton.Color := AColor;
  if not StringToGDKColor(ConfLinkItemFGColor, AColor) then StringToGDKColor(ConfDefaultLinkItemFGColor, AColor);
  LinkItemFGColorButton.Color := AColor;
  if not StringToGDKColor(ConfDotFileItemFGColor, AColor) then StringToGDKColor(ConfDefaultDotFileItemFGColor, AColor);
  DotFileItemFGColorButton.Color := AColor;

  NormalItemFGColorDefault.Checked := ConfNormalItemDefaultColors;
  ActiveItemFGColorDefault.Checked := ConfCursorDefaultColors;
  InactiveItemFGColorDefault.Checked := ConfInactiveItemDefaultColors;
  LinkItemFGColorDefault.Checked := ConfLinkItemDefaultColors;
  DotFileItemFGColorDefault.Checked := ConfDotFileItemDefaultColors;

  SelectAllDirectoriesCheckBox.Checked := ConfSelectAllDirs;
  NewStyleAltOCheckBox.Checked := ConfNewStyleAltO;
  ShowFuncButtonsCheckBox.Checked := ConfShowFuncButtons;
  SizeFormatOptionMenu.ItemIndex := ConfSizeFormat;
  TerminalOptionMenu.ItemIndex := ConfCmdLineTerminalBehaviour;
  DisableFileTipsCheckBox.Checked := ConfDisableFileTips;
  DirsInBoldCheckBox.Checked := ConfDirsInBold;
  DisableDirectoryBracketsCheckBox.Checked := ConfDisableDirectoryBrackets;
  OctalPermissionsCheckBox.Checked := ConfOctalPerm;
  ShowTextUIDsCheckBox.Checked := ConfShowTextUIDs;
  LynxLikeMotionCheckBox.Checked := ConfLynxLikeMotion;
  InsertMovesDownCheckBox.Checked := ConfInsMoveDown;
  SpaceMovesDownCheckBox.Checked := ConfSpaceMovesDown;
  UseInternalViewerCheckBox.Checked := ConfUseInternalViewer;
  ViewerOptionMenu.ItemIndex := ConfViewerTerminalBehaviour;
  EditorOptionMenu.ItemIndex := ConfEditorTerminalBehaviour;
  FocusRefreshCheckBox.Checked := ConfFocusRefresh;
  WMCompatModeCheckBox.Checked := ConfWMCompatMode;
  CompatUseLibcSystemCheckBox.Checked := ConfUseLibcSystem;

  CustomTimeFormatEntry.Text := ConfCustomTimeFormat;
  CustomDateFormatEntry.Text := ConfCustomDateFormat;
  DateFormatOptionMenu.ItemIndex := ConfDateFormat;
  TimeFormatOptionMenu.ItemIndex := ConfTimeFormat;
  DateTimeFormatOptionMenu.ItemIndex := ConfDateTimeFormat;
  QuickSearchOptionMenu.ItemIndex := ConfQuickSearchActivationKey;
  SortDirectoriesLikeFilesCheckBox.Checked := ConfSortDirectoriesLikeFiles;
  VFSTempPathEntry.Text := StrToUTF8(ConfTempPath);
  QuickRenameSkipExtCheckBox.Checked := ConfQuickRenameSkipExt;
  RightClickSelectCheckBox.Checked := ConfRightClickSelect;

  DefaultFontCheckBoxToggled(Self);
  ColorButtonDefaultsToggled(Self);
  UseInternalViewerCheckBoxToggled(Self);
  DateTimeFormatOptionMenuChanged(Self);
end;

procedure TFPreferences.SaveSettings;
var x: integer;
begin
  if not DefaultRowHeightCheckBox.Checked then x := RowHeightSpinEdit.AsInteger
                                          else x := -1;
  RebuildListViews := (ConfRowHeight <> x) or (ConfUseFileTypeIcons <> ShowFiletypeIcons.Checked) or
                      (ConfUseSystemFont <> DefaultFontCheckBox.Checked) or
                      ((not DefaultFontCheckBox.Checked) and (ConfPanelFont <> ListFontPreview.Caption));
  RebuildIcons := ConfRowHeight <> x;

  ConfRowHeight := x;
  ConfNumHistoryItems := NumHistoryItemsSpinEdit.AsInteger;
  ConfClearReadOnlyAttr := ClearROAttr.Checked;
  ConfDisableMouseRename := DisableMouseRename.Checked;
  ConfUseFileTypeIcons := ShowFiletypeIcons.Checked;
  ConfViewer := UTF8ToStr(ViewerCombo.Entry.Text);
  ConfEditor := UTF8ToStr(EditorCombo.Entry.Text);
  ConfTerminalCommand := UTF8ToStr(TerminalCombo.Entry.Text);
  ConfUseSystemFont := DefaultFontCheckBox.Checked;
  if not ConfUseSystemFont then ConfPanelFont := ListFontPreview.Caption;

  ConfNormalItemFGColor := GDKColorToString(NormalItemFGColorButton.Color);
  ConfNormalItemBGColor := GDKColorToString(NormalItemBGColorButton.Color);
  ConfActiveItemFGColor := GDKColorToString(ActiveItemFGColorButton.Color);
  ConfActiveItemBGColor := GDKColorToString(ActiveItemBGColorButton.Color);
  ConfInactiveItemFGColor := GDKColorToString(InactiveItemFGColorButton.Color);
  ConfInactiveItemBGColor := GDKColorToString(InactiveItemBGColorButton.Color);
  ConfSelectedItemFGColor := GDKColorToString(SelectedItemFGColorButton.Color);
  ConfLinkItemFGColor := GDKColorToString(LinkItemFGColorButton.Color);
  ConfDotFileItemFGColor := GDKColorToString(DotFileItemFGColorButton.Color);

  ConfNormalItemDefaultColors := NormalItemFGColorDefault.Checked;
  ConfCursorDefaultColors := ActiveItemFGColorDefault.Checked;
  ConfInactiveItemDefaultColors := InactiveItemFGColorDefault.Checked;
  ConfLinkItemDefaultColors := LinkItemFGColorDefault.Checked;
  ConfDotFileItemDefaultColors := DotFileItemFGColorDefault.Checked;

  ConfSelectAllDirs := SelectAllDirectoriesCheckBox.Checked;
  ConfNewStyleAltO := NewStyleAltOCheckBox.Checked;
  ConfSizeFormat := SizeFormatOptionMenu.ItemIndex;
  ConfCmdLineTerminalBehaviour := TerminalOptionMenu.ItemIndex;
  ConfDirsInBold := DirsInBoldCheckBox.Checked;
  ConfDisableDirectoryBrackets := DisableDirectoryBracketsCheckBox.Checked;
  ConfOctalPerm := OctalPermissionsCheckBox.Checked;
  ConfShowTextUIDs := ShowTextUIDsCheckBox.Checked;
  ConfLynxLikeMotion := LynxLikeMotionCheckBox.Checked;
  ConfInsMoveDown := InsertMovesDownCheckBox.Checked;
  ConfSpaceMovesDown := SpaceMovesDownCheckBox.Checked;
  ConfUseInternalViewer := UseInternalViewerCheckBox.Checked;
  ConfViewerTerminalBehaviour := ViewerOptionMenu.ItemIndex;
  ConfEditorTerminalBehaviour := EditorOptionMenu.ItemIndex;
  ConfFocusRefresh := FocusRefreshCheckBox.Checked;
  ConfWMCompatMode := WMCompatModeCheckBox.Checked;
  ConfUseLibcSystem := CompatUseLibcSystemCheckBox.Checked;
  ConfDisableFileTips := DisableFileTipsCheckBox.Checked;
  ConfShowFuncButtons := ShowFuncButtonsCheckBox.Checked;

  ConfCustomTimeFormat := CustomTimeFormatEntry.Text;
  ConfCustomDateFormat := CustomDateFormatEntry.Text;
  ConfDateFormat := DateFormatOptionMenu.ItemIndex;
  ConfTimeFormat := TimeFormatOptionMenu.ItemIndex;
  ConfDateTimeFormat := DateTimeFormatOptionMenu.ItemIndex;
  ConfQuickSearchActivationKey := QuickSearchOptionMenu.ItemIndex;
  ConfSortDirectoriesLikeFiles := SortDirectoriesLikeFilesCheckBox.Checked;
  ConfTempPath := UTF8ToStr(VFSTempPathEntry.Text);
  ConfQuickRenameSkipExt := QuickRenameSkipExtCheckBox.Checked;
  ConfRightClickSelect := RightClickSelectCheckBox.Checked;
end;

(********************************************************************************************************************************)
end.

