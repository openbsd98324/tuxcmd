(*
    Tux Commander - UProeprties - File properties dialog
    Copyright (C) 2006 Tomas Bzatek <tbzatek@users.sourceforge.net>
    Check for updates on tuxcmd.sourceforge.net

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
unit UProperties;

interface

uses
  glib2, gtk2, pango, SysUtils, Types, Classes, Variants, GTKControls, GTKForms, GTKStdCtrls, GTKExtCtrls, GTKConsts, GTKView,
  GTKUtils, GTKDialogs, GTKPixbuf, GTKClasses, GTKMenus, 
  UCoreClasses, UGnome;

type
  TFProperties = class(TGTKDialog)
    TitleFrame, ListFontFrame: TGTKFrame;
    TitleLabel: TGTKLabel;
    TitleEventBox: TGTKEventBox;
    TitleIcon: TGTKImage;
    TitleHBox: TGTKHBox;
    Notebook: TGTKNotebook;
    BasicPage, PermissionsPage, OpenWithPage: TGTKVBox;
    
    Table2: TGTKTable;
    NameLabel: TGTKLabel;
    NameEdit: TGTKEntry;

    procedure FormCreate(Sender: TObject); override;
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; Key: Word; Shift: TShiftState; var Accept: boolean);
  public
    DisplayFileName: string;
  end;

var
  FProperties: TFProperties;

implementation

uses ULocale, UFileAssoc, UCoreUtils, UConfig;


procedure TFProperties.FormCreate(Sender: TObject);
var i: integer;
begin
  SetDefaultSize(450, 500);
  Caption := Format('%s Properties', [DisplayFileName]);
  Buttons := [mbOK, mbCancel];
  ShowSeparator := False;
  TitleEventBox := TGTKEventBox.Create(Self);
  TitleLabel := TGTKLabel.Create(Self);
  TitleLabel.Caption := Format('<span size="x-large" weight="ultrabold">%s</span>', [Format('%s Properties', [DisplayFileName])]);
  TitleLabel.UseMarkup := True;
  TitleLabel.XAlign := 0;
  TitleLabel.XPadding := 0;
  TitleLabel.YPadding := 3;
  TitleEventBox.ControlState := csPrelight;
  TitleFrame := TGTKFrame.CreateWithoutLabel(Self);
  TitleFrame.ShadowType := stShadowOut;
  TitleIcon := TGTKImage.Create(Self);
  TitleIcon.SetFromStock('gtk-properties', isLargeToolbar);
  TitleHBox := TGTKHBox.Create(Self);
  TitleHBox.Homogeneous := False;
  TitleHBox.AddControlEx(TGTKVBox.Create(Self), False, False, 5);
  TitleHBox.AddControlEx(TitleIcon, False, False, 0);
  TitleHBox.AddControlEx(TitleLabel, True, True, 10);
  TitleEventBox.AddControl(TitleHBox);
  TitleFrame.AddControl(TitleEventBox);
  ClientArea.AddControlEx(TitleFrame, False, True, 0);

  Notebook := TGTKNotebook.Create(Self);
  Notebook.BorderWidth := 10;
  ClientArea.AddControlEx(Notebook, True, True, 0);

  BasicPage := TGTKVBox.Create(Notebook);
  Notebook.AppendPage(BasicPage, 'Basic');

  PermissionsPage := TGTKVBox.Create(Notebook);
  Notebook.AppendPage(PermissionsPage, 'Permissions');

  OpenWithPage := TGTKVBox.Create(Notebook);
  Notebook.AppendPage(OpenWithPage, 'Open With');


  // *********  PAGE Basic

  BasicPage.AddControlEx(TGTKVBox.Create(Self), False, False, 5);
  Table2 := TGTKTable.Create(Self);
  Table2.SetRowColCount(8, 5);
  BasicPage.AddControlEx(Table2, False, False, 0);

  NameLabel := TGTKLabel.Create(Self);
  NameLabel.Caption := Format('<span weight="ultrabold">%s</span>', ['_Name:']);
  NameLabel.XAlign := 0;
  NameLabel.UseMarkup := True;
  NameEdit := TGTKEntry.Create(Self);
  NameLabel.FocusControl := NameEdit;
  NameLabel.UseUnderline := True;
  Table2.AddControlEx(0, 0, 1, 1, NameLabel, [taoShrink, taoFill], [taoShrink], 2, 5);
  Table2.AddControlEx(1, 0, 1, 1, NameEdit, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 2, 5);



  // *********  PAGE Permissions



  // *********  PAGE Applications


  // ******************

  OnKeyDown := FormKeyDown;
  OnDestroy := FormDestroy;
end;

procedure TFProperties.FormKeyDown(Sender: TObject; Key: Word; Shift: TShiftState; var Accept: boolean);
begin
  if Key = GDK_ESCAPE then ModalResult := mbCancel;
end;

procedure TFProperties.FormDestroy(Sender: TObject);
begin

end;

(********************************************************************************************************************************)
end.

