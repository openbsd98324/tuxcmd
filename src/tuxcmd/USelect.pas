(*
    Tux Commander - USelect - Select pattern dialog and related funcions 
    Copyright (C) 2008 Tomas Bzatek <tbzatek@users.sourceforge.net>
    Check for updates on tuxcmd.sourceforge.net

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
unit USelect;

interface

uses
  glib2, gtk2, pango, SysUtils, Types, Classes, GTKControls, GTKForms, GTKStdCtrls, GTKExtCtrls, GTKConsts, GTKView,
  GTKUtils, GTKDialogs, GTKPixbuf, GTKClasses;
  
type
  TFSelect = class(TGTKDialog)
{    TitleFrame, ListFontFrame: TGTKFrame;
    TitleLabel: TGTKLabel;
    TitleEventBox: TGTKEventBox;
    TitleIcon: TGTKImage;
    TitleHBox: TGTKHBox; }

    Label1: TGTKLabel;
    ComboBox: TGTKCombo;
    Box: TGTKVBox;
    procedure FormCreate(Sender: TObject); override;
    procedure FormKeyDown(Sender: TObject; Key: Word; Shift: TShiftState; var Accept: boolean);
    procedure ComboBoxKeyDown(Sender: TObject; Key: Word; Shift: TShiftState; var Accept: boolean);
  private
    SavedData: string;
  end;

var
  FSelect: TFSelect;

implementation

uses ULocale, UCore;


procedure TFSelect.FormCreate(Sender: TObject);
var i: integer;
begin
  SetDefaultSize(300, 50);
  Buttons := [mbOK, mbCancel];

{  TitleEventBox := TGTKEventBox.Create(Self);
  TitleLabel := TGTKLabel.Create(Self);
  TitleLabel.Caption := '<span size="large" weight="ultrabold">File Properties</span>';
  TitleLabel.UseMarkup := True;
  TitleLabel.XAlign := 0;
  TitleLabel.XPadding := 0;
  TitleLabel.YPadding := 3;
  TitleEventBox.ControlState := csPrelight;
  TitleFrame := TGTKFrame.CreateWithoutLabel(Self);
  TitleFrame.ShadowType := stShadowOut;
  TitleIcon := TGTKImage.Create(Self);
  TitleIcon.SetFromStock('gtk-edit', isSmallToolbar);
  TitleHBox := TGTKHBox.Create(Self);
  TitleHBox.Homogeneous := False;
  TitleHBox.AddControlEx(TGTKVBox.Create(Self), False, False, 5);
  TitleHBox.AddControlEx(TitleIcon, False, False, 0);
  TitleHBox.AddControlEx(TitleLabel, True, True, 10);
  TitleEventBox.AddControl(TitleHBox);
  TitleFrame.AddControl(TitleEventBox);
  ClientArea.AddControlEx(TitleFrame, False, True, 0);
  ClientArea.AddControlEx(TGTKHBox.Create(Self), False, False, 5);  }


  Box := TGTKVBox.Create(Self);
  Label1 := TGTKLabel.Create(Self);
  Label1.Caption := LANGSpecifyFileType;
  Label1.UseUnderline := True;
  Label1.XAlign := 0;
  Label1.XPadding := 0;
  ComboBox := TGTKCombo.Create(Self);
  ComboBox.DisableActivate;
  if SelectHistory.Count > 0 then
    for i := 0 to SelectHistory.Count - 1 do
      ComboBox.Items.Append(SelectHistory[i]);
  ComboBox.Entry.Text := '*.*';
  ComboBox.Entry.OnKeyDown := ComboBoxKeyDown;
  Label1.FocusControl := ComboBox.Entry;
  Box.AddControlEx(Label1, False, False, 0);
  Box.AddControlEx(ComboBox, False, False, 3);
  Box.BorderWidth := 12;
  ClientArea.AddControlEx(Box, True, True, 0);
  OnKeyDown := FormKeyDown;
  ComboBox.SetFocus;
end;

procedure TFSelect.FormKeyDown(Sender: TObject; Key: Word; Shift: TShiftState; var Accept: boolean);
begin
  case Key of
    GDK_RETURN, GDK_KP_ENTER: ModalResult := mbOK;
    GDK_ESCAPE: ModalResult := mbCancel;
  end;
end;

procedure TFSelect.ComboBoxKeyDown(Sender: TObject; Key: Word; Shift: TShiftState; var Accept: boolean);
var Orig, s: string;
    i: integer;
begin
  case Key of
    GDK_UP, GDK_DOWN: if Shift = [] then begin
                        Accept := False;
                        if SelectHistory.Count > 0 then begin
                          Orig := Trim(ComboBox.Entry.Text);
                          i := SelectHistory.IndexOf(Orig);

                          if Key = GDK_DOWN then begin
                            if i < 0 then begin
                              SavedData := Orig;
                              i := 0;
                            end else
                            if SelectHistory.Count > i + 1 then Inc(i);
                            s := SelectHistory[i];
                          end else begin
                            if i < 0 then Exit else
                            if i = 0 then begin
                              s := SavedData;
                              SavedData := '';
                            end else
                            if SelectHistory.Count > i then s := SelectHistory[i - 1];
                          end;
                          
                          ComboBox.Entry.Text := s;
                          ComboBox.Entry.SetFocus;
                          ComboBox.Entry.SelectAll;
                        end;
                      end;
  end;
end;



end.

