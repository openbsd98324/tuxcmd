(*
    Tux Commander - USplitFile - Split File dialog
    Copyright (C) 2004 Tomas Bzatek <tbzatek@users.sourceforge.net>
    Check for updates on tuxcmd.sourceforge.net

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
unit USplitFile;

interface

uses
  SysUtils, Types, Classes, Variants, GTKControls, GTKForms, GTKStdCtrls, GTKExtCtrls, GTKConsts;

type
  TFSplitFile = class(TGTKDialog)
    Label1, Label2: TGTKLabel;
    Entry: TGTKEntry;
    Box: TGTKVBox;
    SizeBox: TGTKHBox;
    SizeCombo: TGTKCombo;
    DeleteTargetCheckBox: TGTKCheckButton;
    procedure FormCreate(Sender: TObject); override;
    procedure FormKeyDown(Sender: TObject; Key: Word; Shift: TShiftState; var Accept: boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FSplitFile: TFSplitFile;

type TSplitRecord = record
                      Title: string;
                      PartSize: Int64;
                    end;
const SplitConsts: array[1..8] of TSplitRecord = (
         (Title: '100 MB (ZIP)'; PartSize: 100431872),
         (Title: '250 MB (ZIP)'; PartSize: 250331136),
         (Title: '1.44 MB (3.5")'; PartSize: 1457664),
         (Title: '1.2 MB (5.25")'; PartSize: 1213952),
         (Title: '720 kB (3.5")'; PartSize: 730112),
         (Title: '360 kB (5.25")'; PartSize: 362496),
         (Title: '650 MB (CD-R)'; PartSize: 681574400),
         (Title: '700 MB (CD-R)'; PartSize: 734003200));

implementation

uses ULocale;


procedure TFSplitFile.FormCreate(Sender: TObject);
var i: integer;
begin
  SetDefaultSize(400, -1);
  Caption := LANGSplitFile;
  Buttons := [mbOK, mbCancel];
  Box := TGTKVBox.Create(Self);
  Label1 := TGTKLabel.Create(Self);
  Label1.XAlign := 0;
  Label1.XPadding := 0;
  Entry := TGTKEntry.Create(Self);
  Label1.FocusControl := Entry;
  Box.AddControlEx(Label1, False, False, 0);
  Box.AddControlEx(Entry, False, False, 0);
  Box.BorderWidth := 8;
  ClientArea.AddControlEx(Box, True, True, 0);
  SizeBox := TGTKHBox.Create(Self);
  SizeBox.Homogeneous := False;
  Label2 := TGTKLabel.Create(Self);
  Label2.XAlign := 0;
  Label2.XPadding := 10;
  Label2.Caption := LANGBytesPerFile;
  SizeCombo := TGTKCombo.Create(Self);
  Label2.FocusControl := SizeCombo.Entry;
  Label2.UseUnderline := True;
  SizeCombo.Items.Append(LANGAutomatic);
  for i := 1 to Length(SplitConsts) do
    SizeCombo.Items.Append(SplitConsts[i].Title);
  SizeCombo.Entry.Text := LANGAutomatic;
  SizeBox.AddControlEx(Label2, False, False, 0);
  SizeBox.AddControlEx(SizeCombo, False, False, 5);
  SizeBox.AddControlEx(TGTKLabel.Create(Self), True, True, 0);
  Box.AddControlEx(TGTKVBox.Create(Self), False, False, 3);
  Box.AddControlEx(SizeBox, False, False, 0);
  DeleteTargetCheckBox := TGTKCheckButton.CreateWithLabel(Self, LANGDeleteFilesOnTargetDisk);
  Box.AddControlEx(DeleteTargetCheckBox, False, False, 3); 
  OnKeyDown := FormKeyDown;
  Entry.SetFocus;
end;

procedure TFSplitFile.FormKeyDown(Sender: TObject; Key: Word; Shift: TShiftState; var Accept: boolean);
begin
  case Key of
    GDK_RETURN, GDK_KP_ENTER: ModalResult := mbOK;
    GDK_ESCAPE: ModalResult := mbCancel;
  end;
end;


end.

