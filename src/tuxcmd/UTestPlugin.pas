(*
    Tux Commander - UTestPlugin - Plugin testing
    Copyright (C) 2004 Tomas Bzatek <tbzatek@users.sourceforge.net>
    Check for updates on tuxcmd.sourceforge.net

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
unit UTestPlugin;

interface

uses
  glib2, gdk2, gtk2, pango, SysUtils, Types, Classes, Variants, GTKControls, GTKForms, GTKStdCtrls, GTKExtCtrls, GTKConsts, GTKView,
  GTKUtils, GTKDialogs, GTKPixbuf, GTKClasses, GTKMenus,
  UCoreClasses;

type
  TFTestPlugin = class(TGTKDialog)
    TitleFrame: TGTKFrame;
    TitleLabel: TGTKLabel;
    TitleEventBox: TGTKEventBox;
    TitleIcon: TGTKImage;
    TitleHBox: TGTKHBox;
    Table: TGTKTable;
    Label1, Label2, Label3, Label4: TGTKLabel;
    PluginOptionMenu: TGTKOptionMenu;
    CommandEntry, UserEntry, PasswordEntry: TGTKEntry;
    ExperimentalWarningLabel: TGTKLabel;
    ExperimentalWarningIcon: TGTKImage;
    AnonymousCheckButton: TGTKCheckButton;
    HBox4: TGTKHBox;
    procedure FormCreate(Sender: TObject); override;
    procedure FormKeyDown(Sender: TObject; Key: Word; Shift: TShiftState; var Accept: boolean);
    procedure AnonymousCheckButtonToggled(Sender: TObject);
  end;

var
  FTestPlugin: TFTestPlugin;

implementation

uses UVFSCore, ULocale;



procedure TFTestPlugin.FormCreate(Sender: TObject);
var i: integer;
    MenuItem: TGTKMenuItem;
begin
  SetDefaultSize(400, 200);
  Caption := LANGTestPlugin_Caption;
  Buttons := [mbOK, mbCancel];
  DefaultButton := mbOK;
  ShowSeparator := False;
  TitleEventBox := TGTKEventBox.Create(Self);
  TitleLabel := TGTKLabel.Create(Self);
  TitleLabel.Caption := Format('<span size="x-large" weight="ultrabold">%s</span>', [LANGTestPlugin_Title]);
  TitleLabel.UseMarkup := True;
  TitleLabel.XAlign := 0;
  TitleLabel.XPadding := 0;
  TitleLabel.YPadding := 3;
  TitleEventBox.ControlState := csPrelight;
  TitleFrame := TGTKFrame.CreateWithoutLabel(Self);
  TitleFrame.ShadowType := stShadowOut;
  TitleIcon := TGTKImage.Create(Self);
  TitleIcon.SetFromStock('gtk-justify-center', isLargeToolbar);
  TitleHBox := TGTKHBox.Create(Self);
  TitleHBox.Homogeneous := False;
  TitleHBox.AddControlEx(TGTKEventBox.Create(Self), False, False, 5);
  TitleHBox.AddControlEx(TitleIcon, False, False, 0);
  TitleHBox.AddControlEx(TitleLabel, True, True, 10);
  TitleEventBox.AddControl(TitleHBox);
  TitleFrame.AddControl(TitleEventBox);
  ClientArea.AddControlEx(TitleFrame, False, True, 0);

  HBox4 := TGTKHBox.Create(Self);
  HBox4.Homogeneous := False;
  ExperimentalWarningLabel := TGTKLabel.Create(Self);
  ExperimentalWarningLabel.Caption := LANGTestPlugin_ExperimentalWarningLabelCaption; 
  ExperimentalWarningLabel.UseMarkup := True;
  ExperimentalWarningLabel.LineWrap := True;
  ExperimentalWarningLabel.SetSizeRequest(300, -1);
  ExperimentalWarningIcon := TGTKImage.Create(Self);
  ExperimentalWarningIcon.SetFromStock('gtk-dialog-warning', isDialog);
  HBox4.AddControlEx(TGTKEventBox.Create(Self), False, False, 5);
  HBox4.AddControlEx(ExperimentalWarningIcon, False, False, 7);
  HBox4.AddControlEx(ExperimentalWarningLabel, True, True, 7);
  HBox4.AddControlEx(TGTKEventBox.Create(Self), False, False, 8);

  Table := TGTKTable.Create(Self);
  Table.BorderWidth := 20;
  ClientArea.AddControlEx(Table, True, True, 0);
  Table.AddControlEx(0, 0, 2, 1, HBox4, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 5, 2);
  Table.AddControlEx(0, 1, 2, 1, TGTKEventBox.Create(Self), [taoShrink, taoFill], [taoShrink], 5, 10);
  Label1 := TGTKLabel.Create(Self);
  Label1.XAlign := 0;
  Label1.Caption := LANGTestPlugin_Plugin;
  Label2 := TGTKLabel.Create(Self);
  Label2.XAlign := 0;
  Label2.Caption := LANGTestPlugin_Command;
  PluginOptionMenu := TGTKOptionMenu.Create(Self);
  Label1.FocusControl := PluginOptionMenu;
  Label1.UseUnderline := True;
  CommandEntry := TGTKEntry.Create(Self);
  Label2.FocusControl := CommandEntry;
  Label2.UseUnderline := True;
  Table.AddControlEx(0, 2, 1, 1, Label1, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 5, 2);
  Table.AddControlEx(0, 3, 1, 1, Label2, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 5, 2);
  Table.AddControlEx(1, 2, 1, 1, PluginOptionMenu, [taoExpand, taoFill], [taoShrink], 5, 2);
  Table.AddControlEx(1, 3, 1, 1, CommandEntry, [taoExpand, taoFill], [taoShrink], 5, 2);
  Label3 := TGTKLabel.Create(Self);
  Label3.XAlign := 0;
  Label3.Caption := LANGTestPlugin_Username;
  UserEntry := TGTKEntry.Create(Self);
  Label3.FocusControl := UserEntry;
  Label3.UseUnderline := True;
  Label4 := TGTKLabel.Create(Self);
  Label4.XAlign := 0;
  Label4.Caption := LANGTestPlugin_Password;
  PasswordEntry := TGTKEntry.Create(Self);
  Label4.FocusControl := PasswordEntry;
  Label4.UseUnderline := True;
  AnonymousCheckButton := TGTKCheckButton.CreateWithLabel(Self, LANGTestPlugin_AnonymousCheckButton);
  AnonymousCheckButton.OnToggled := AnonymousCheckButtonToggled;
  AnonymousCheckButton.Checked := True;
  Table.AddControlEx(0, 4, 2, 1, TGTKEventBox.Create(Self), [taoShrink, taoFill], [taoExpand, taoFill], 5, 4);
  Table.AddControlEx(0, 5, 1, 1, Label3, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 5, 2);
  Table.AddControlEx(1, 5, 1, 1, UserEntry, [taoExpand, taoFill], [taoShrink], 5, 2);
  Table.AddControlEx(0, 6, 1, 1, Label4, [taoShrink, taoFill], [taoShrink, taoExpand, taoFill], 5, 2);
  Table.AddControlEx(1, 6, 1, 1, PasswordEntry, [taoExpand, taoFill], [taoShrink], 5, 2);
  Table.AddControlEx(0, 7, 2, 1, AnonymousCheckButton, [taoExpand, taoFill], [taoShrink], 20, 2);



  if PluginList.Count = 0 then begin
    MenuItem := TGTKMenuItem.CreateTyped(Self, itLabel);
    MenuItem.Caption := LANGTestPlugin_NoPluginsFound;
    MenuItem.Enabled := False;
    PluginOptionMenu.Items.Add(MenuItem);
  end else
    for i := 0 to PluginList.Count - 1 do begin
      MenuItem := TGTKMenuItem.CreateTyped(Self, itImageText);
      MenuItem.SetCaptionPlain(Format('%s [%s]', [TVFSPlugin(PluginList[i]).ModuleName,
                                                  ExtractFileName(TVFSPlugin(PluginList[i]).FullModulePath)]));
      PluginOptionMenu.Items.Add(MenuItem);
    end;

  OnKeyDown := FormKeyDown;
end;

procedure TFTestPlugin.FormKeyDown(Sender: TObject; Key: Word; Shift: TShiftState; var Accept: boolean);
begin
  case Key of
    GDK_RETURN, GDK_KP_ENTER: ModalResult := mbOK;
    GDK_ESCAPE: ModalResult := mbCancel;
  end;
end;

procedure TFTestPlugin.AnonymousCheckButtonToggled(Sender: TObject);
begin
  Label3.Enabled := not AnonymousCheckButton.Checked;
  Label4.Enabled := not AnonymousCheckButton.Checked;
  UserEntry.Enabled := not AnonymousCheckButton.Checked;
  PasswordEntry.Enabled := not AnonymousCheckButton.Checked;
end;


end.

