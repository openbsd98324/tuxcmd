(*
    GTK-Kylix Library: GTKDialogs - Special purpose dialogs 
    Version 0.7.0  (last updated 2006-02-05)
    Copyright (C) 2006 Tomas Bzatek <tbzatek@users.sourceforge.net>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the 
    Free Software Foundation, Inc., 59 Temple Place - Suite 330, 
    Boston, MA  02111-1307  USA.

*)

unit GTKDialogs;

interface

uses gtk2, gdk2, glib2, Classes, GTKControls, GTKConsts, GTKUtils, GTKClasses, GTKForms;


type

(****************************************** TGTKFILESELECTIONDIALOG *************************************************************)
  TGTKFileSelectionDialog = class(TGTKDialog)
  private
    function GetFileName: string;
    function GetShowFileOpButtons: boolean;
    function GetMultiSelect: boolean;
    procedure SetFileName(Value: string);
    procedure SetShowFileOpButtons(Value: boolean);
    procedure SetMultiSelect(Value: boolean);
  protected
  public
    constructor Create(AOwner: TComponent); override;
    constructor CreateWithTitle(AOwner: TComponent; const Title: string);
    destructor Destroy; override;
  published
    property FileName: string read GetFileName write SetFileName;
    property ShowFileOpButtons: boolean read GetShowFileOpButtons write SetShowFileOpButtons;
    property MultiSelect: boolean read GetMultiSelect write SetMultiSelect;
  end;

(****************************************** TGTKCOLORSELECTIONDIALOG ************************************************************)
  TGTKColorSelectionDialog = class(TGTKDialog)
  private
    function GetShowOpacity: boolean;
    procedure SetShowOpacity(Value: boolean);
    function GetShowPalette: boolean;
    procedure SetShowPalette(Value: boolean);
    function GetColor: TGDKColor;
    procedure SetColor(Value: TGDKColor);
  protected
  public
    constructor Create(AOwner: TComponent); override;
    constructor CreateWithTitle(AOwner: TComponent; const Title: string);
    destructor Destroy; override;
    property Color: TGDKColor read GetColor write SetColor;
  published
    property ShowOpacity: boolean read GetShowOpacity write SetShowOpacity;
    property ShowPalette: boolean read GetShowPalette write SetShowPalette;
  end;

(****************************************** TGTKFONTSELECTIONDIALOG *************************************************************)
  TGTKFontSelectionDialog = class(TGTKDialog)
  private
    function GetFontName: string;
    procedure SetFontName(Value: string);
    function GetPreviewText: string;
    procedure SetPreviewText(Value: string);
  protected
  public
    constructor Create(AOwner: TComponent); override;
    constructor CreateWithTitle(AOwner: TComponent; const Title: string);
    destructor Destroy; override;
  published
    property FontName: string read GetFontName write SetFontName;
    property PreviewText: string read GetPreviewText write SetPreviewText;
  end;

(********************************************************************************************************************************)
(********************************************************************************************************************************)
implementation

uses SysUtils, DateUtils;


(********************************************************************************************************************************)
(********************************************************************************************************************************)
constructor TGTKFileSelectionDialog.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FWidget := gtk_file_selection_new(nil);
  Show;
end;

constructor TGTKFileSelectionDialog.CreateWithTitle(AOwner: TComponent; const Title: string);
begin
  inherited Create(AOwner);
  FWidget := gtk_file_selection_new(StringToPgchar(Title));
  Show;
end;

destructor TGTKFileSelectionDialog.Destroy;
begin
  inherited Destroy;
end;

function TGTKFileSelectionDialog.GetFileName: string;
begin
  Result := string(gtk_file_selection_get_filename(PGtkFileSelection(FWidget)));
end;

procedure TGTKFileSelectionDialog.SetFileName(Value: string);
begin
  gtk_file_selection_set_filename(PGtkFileSelection(FWidget), PChar(Value));
end;

function TGTKFileSelectionDialog.GetShowFileOpButtons: boolean;
var b: Boolean;
begin
  g_object_get(FWidget, 'show-fileops', @b, nil);
  Result := b;
end;

procedure TGTKFileSelectionDialog.SetShowFileOpButtons(Value: boolean);
begin
  if Value then gtk_file_selection_show_fileop_buttons(PGtkFileSelection(FWidget))
           else gtk_file_selection_hide_fileop_buttons(PGtkFileSelection(FWidget));
end;

function TGTKFileSelectionDialog.GetMultiSelect: boolean;
begin
  Result := gtk_file_selection_get_select_multiple(PGtkFileSelection(FWidget));
end;

procedure TGTKFileSelectionDialog.SetMultiSelect(Value: boolean);
begin
  gtk_file_selection_set_select_multiple(PGtkFileSelection(FWidget), Value);
end;

(********************************************************************************************************************************)
(********************************************************************************************************************************)
constructor TGTKColorSelectionDialog.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FWidget := gtk_color_selection_dialog_new(nil);
  Show;
end;

constructor TGTKColorSelectionDialog.CreateWithTitle(AOwner: TComponent; const Title: string);
begin
  inherited Create(AOwner);
  FWidget := gtk_color_selection_dialog_new(StringToPgchar(Title));
  Show;
end;

destructor TGTKColorSelectionDialog.Destroy;
begin
  inherited Destroy;
end;

function TGTKColorSelectionDialog.GetShowOpacity: boolean;
begin
  Result := gtk_color_selection_get_has_opacity_control(PGtkColorSelection(PGtkColorSelectionDialog(FWidget)^.colorsel));
end;

procedure TGTKColorSelectionDialog.SetShowOpacity(Value: boolean);
begin
  gtk_color_selection_set_has_opacity_control(PGtkColorSelection(PGtkColorSelectionDialog(FWidget)^.colorsel), Value);
end;

function TGTKColorSelectionDialog.GetShowPalette: boolean;
begin
  Result := gtk_color_selection_get_has_palette(PGtkColorSelection(PGtkColorSelectionDialog(FWidget)^.colorsel));
end;

procedure TGTKColorSelectionDialog.SetShowPalette(Value: boolean);
begin
  gtk_color_selection_set_has_palette(PGtkColorSelection(PGtkColorSelectionDialog(FWidget)^.colorsel), Value);
end;

function TGTKColorSelectionDialog.GetColor: TGDKColor;
var Col: gdk2.TGDkColor;
begin
  gtk_color_selection_get_current_color(PGtkColorSelection(PGtkColorSelectionDialog(FWidget)^.colorsel), @Col);
  Result := PGdkColorToGDKColor(@Col);
end;

procedure TGTKColorSelectionDialog.SetColor(Value: TGDKColor);
var Col: PGDkColor;
begin
  Col := GDKColorToPGdkColor(Value);
  gtk_color_selection_set_current_color(PGtkColorSelection(PGtkColorSelectionDialog(FWidget)^.colorsel), Col);
end;

(********************************************************************************************************************************)
(********************************************************************************************************************************)
constructor TGTKFontSelectionDialog.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FWidget := gtk_font_selection_dialog_new(nil);
  Show;
end;

constructor TGTKFontSelectionDialog.CreateWithTitle(AOwner: TComponent; const Title: string);
begin
  inherited Create(AOwner);
  FWidget := gtk_font_selection_dialog_new(StringToPgchar(Title));
  Show;
end;

destructor TGTKFontSelectionDialog.Destroy;
begin
  inherited Destroy;
end;

function TGTKFontSelectionDialog.GetFontName: string;
begin
  Result := PgcharToString(gtk_font_selection_dialog_get_font_name(PGtkFontSelectionDialog(FWidget)));
end;

procedure TGTKFontSelectionDialog.SetFontName(Value: string);
begin
  gtk_font_selection_dialog_set_font_name(PGtkFontSelectionDialog(FWidget), StringToPgchar(Value));
end;

function TGTKFontSelectionDialog.GetPreviewText: string;
begin
  Result := PgcharToString(gtk_font_selection_dialog_get_preview_text(PGtkFontSelectionDialog(FWidget)));
end;

procedure TGTKFontSelectionDialog.SetPreviewText(Value: string);
begin
  gtk_font_selection_dialog_set_preview_text(PGtkFontSelectionDialog(FWidget), StringToPgchar(Value));
end;

(********************************************************************************************************************************)
end.
