(*
    GTK-Kylix Library: GTKText - Multiline Text Editor (GtkTextView) 
    Version 0.5.16  (last updated 2003-01-21)
    Copyright (C) 2003 Tomas Bzatek <tbzatek@users.sourceforge.net>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the 
    Free Software Foundation, Inc., 59 Temple Place - Suite 330, 
    Boston, MA  02111-1307  USA.

*)

unit GTKText;

interface

uses gtk2, gdk2, glib2, Classes, GTKControls, GTKConsts, GTKUtils, GTKClasses, GTKForms;


type
  TGTKTextBuffer = class;

(****************************************** TGTKTEXTVIEW ************************************************************************)
  TGTKTextViewWrapMode = (wmWrapNone, wmWrapChar, wmWrapWord);
  TGTKTextView = class(TGTKContainer)
  private
    FTextBuffer: TGTKTextBuffer;
    function GetWrapMode: TGTKTextViewWrapMode;
    procedure SetWrapMode(Value: TGTKTextViewWrapMode);
    function GetReadOnly: boolean;
    procedure SetReadOnly(Value: boolean);
    function GetCursorVisible: boolean;
    procedure SetCursorVisible(Value: boolean);
  protected
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property WrapMode: TGTKTextViewWrapMode read GetWrapMode write SetWrapMode;
    property ReadOnly: boolean read GetReadOnly write SetReadOnly;
    property CursorVisible: boolean read GetCursorVisible write SetCursorVisible;
    property TextBuffer: TGTKTextBuffer read FTextBuffer write FTextBuffer;
    property Lines: TGTKTextBuffer read FTextBuffer write FTextBuffer;
  end;

(****************************************** TGTKTEXTBUFFER **********************************************************************)
  TGTKTextBuffer = class
  private
    FOwner: TGTKTextView;
    FBuffer: PGtkTextBuffer;
    function GetLineCount: integer;
    function GetCharCount: integer;
  protected
  public
    constructor Create(AOwner: TGTKTextView);
    destructor Destroy; override;
    procedure SetText(const Text: string);
    procedure InsertText(const Text: string);
  published
    property Count: integer read GetLineCount;
    property LineCount: integer read GetLineCount;
    property CharCount: integer read GetCharCount;
  end;


(********************************************************************************************************************************)
(********************************************************************************************************************************)
implementation

uses SysUtils, DateUtils;


(********************************************************************************************************************************)
(********************************************************************************************************************************)
constructor TGTKTextView.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FTextBuffer := TGTKTextBuffer.Create(Self);
  FWidget := gtk_text_view_new_with_buffer(FTextBuffer.FBuffer);
  Show;
  g_object_unref(FTextBuffer.FBuffer);
end;

destructor TGTKTextView.Destroy;
begin
  inherited Destroy;
end;

function TGTKTextView.GetWrapMode: TGTKTextViewWrapMode;
begin
  Result := TGTKTextViewWrapMode(gtk_text_view_get_wrap_mode(PGtkTextView(FWidget)));
end;

procedure TGTKTextView.SetWrapMode(Value: TGTKTextViewWrapMode);
begin
  gtk_text_view_set_wrap_mode(PGtkTextView(FWidget), integer(Value));
end;

function TGTKTextView.GetReadOnly: boolean;
begin
  Result := not gtk_text_view_get_editable(PGtkTextView(FWidget));
end;

procedure TGTKTextView.SetReadOnly(Value: boolean);
begin
  gtk_text_view_set_editable(PGtkTextView(FWidget), not Value);
end;

function TGTKTextView.GetCursorVisible: boolean;
begin
  Result := gtk_text_view_get_cursor_visible(PGtkTextView(FWidget));
end;

procedure TGTKTextView.SetCursorVisible(Value: boolean);
begin
  gtk_text_view_set_cursor_visible(PGtkTextView(FWidget), Value);
end;

(********************************************************************************************************************************)
(********************************************************************************************************************************)
constructor TGTKTextBuffer.Create(AOwner: TGTKTextView);
begin
  inherited Create;
  FOwner := AOwner;
  FBuffer := gtk_text_buffer_new(nil);
end;

destructor TGTKTextBuffer.Destroy;
begin
  inherited Destroy;
end;

procedure TGTKTextBuffer.SetText(const Text: string);
begin
  gtk_text_buffer_set_text(FBuffer, PChar(Text), -1);
end;

procedure TGTKTextBuffer.InsertText(const Text: string);
var Iter: TGtkTextIter;
begin
  gtk_text_buffer_get_end_iter(FBuffer, @Iter);
  gtk_text_buffer_insert(FBuffer, @Iter, PChar(Text), -1);
end;

function TGTKTextBuffer.GetLineCount: integer;
begin
  Result := gtk_text_buffer_get_line_count(FBuffer);
end;

function TGTKTextBuffer.GetCharCount: integer;
begin
  Result := gtk_text_buffer_get_char_count(FBuffer);
end;

(********************************************************************************************************************************)
end.
