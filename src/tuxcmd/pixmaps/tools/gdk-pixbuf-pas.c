#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>

#include <glib.h>

#include "emblem_symbolic_link_png.c"
#include "gnome_dev_cdrom_16_png.c"
#include "gnome_dev_floppy_16_png.c"
#include "gnome_dev_harddisk_16_png.c"
#include "gnome_dev_removable_usb_16_png.c"
#include "gnome_mime_application_zip_16_png.c"
#include "gnome_mime_x_directory_smb_share_16_png.c"
#include "stock_folder_16_png.c"
#include "stock_lock_16_png.c"
#include "stock_lock_48_png.c"
#include "stock_new_16_png.c"
#include "stock_up_one_dir_16_png.c"
#include "tuxcmd_16_png.c"
#include "tuxcmd_24_png.c"
#include "tuxcmd_32_png.c"
#include "tuxcmd_48_png.c"
#include "tuxcmd_64_png.c"
#include "tuxcmd_128_png.c"


#define HEADER_BYTES    24
#define LINE_ELEMENTS   16
#define NEW_LINE        "\n    "


void
dump_pas_struct (const guint8 *data, const guint32 len, const char *name)
{
  guint32 i;
  guint32 counter = 1;

  g_print ("const %s: array[1..%d] of byte = (%s", name, len, NEW_LINE);
  for (i = 0; i < len; i++) {
    if ((i == 4) || (i == 8) || (i == 12) || (i == 16) || (i == 20) || (i == 24)) {
      g_print (NEW_LINE);
    }
    g_print ("$%.2X", *(guint8*) (data + i));
    if (i < len - 1)
      g_print (", ");
    if (i >= HEADER_BYTES) {
      if ((counter % LINE_ELEMENTS) == 0)
        g_print (NEW_LINE);
      counter++;
    }
  }
  g_print (");\n\n");
}

int
main (int argc, char* argv[])
{
  dump_pas_struct (&emblem_symbolic_link_png[0], G_N_ELEMENTS (emblem_symbolic_link_png), "emblem_symbolic_link_png");
  dump_pas_struct (&gnome_dev_cdrom_16_png[0], G_N_ELEMENTS (gnome_dev_cdrom_16_png), "gnome_dev_cdrom_16_png");
  dump_pas_struct (&gnome_dev_floppy_16_png[0], G_N_ELEMENTS (gnome_dev_floppy_16_png), "gnome_dev_floppy_16_png");
  dump_pas_struct (&gnome_dev_harddisk_16_png[0], G_N_ELEMENTS (gnome_dev_harddisk_16_png), "gnome_dev_harddisk_16_png");
  dump_pas_struct (&gnome_dev_removable_usb_16_png[0], G_N_ELEMENTS (gnome_dev_removable_usb_16_png), "gnome_dev_removable_usb_16_png");
  dump_pas_struct (&gnome_mime_application_zip_16_png[0], G_N_ELEMENTS (gnome_mime_application_zip_16_png), "gnome_mime_application_zip_16_png");
  dump_pas_struct (&gnome_mime_x_directory_smb_share_16_png[0], G_N_ELEMENTS (gnome_mime_x_directory_smb_share_16_png), "gnome_mime_x_directory_smb_share_16_png");
  dump_pas_struct (&stock_folder_16_png[0], G_N_ELEMENTS (stock_folder_16_png), "stock_folder_16_png");
  dump_pas_struct (&stock_lock_16_png[0], G_N_ELEMENTS (stock_lock_16_png), "stock_lock_16_png");
  dump_pas_struct (&stock_lock_48_png[0], G_N_ELEMENTS (stock_lock_48_png), "stock_lock_48_png");
  dump_pas_struct (&stock_new_16_png[0], G_N_ELEMENTS (stock_new_16_png), "stock_new_16_png");
  dump_pas_struct (&stock_up_one_dir_16_png[0], G_N_ELEMENTS (stock_up_one_dir_16_png), "stock_up_one_dir_16_png");
  dump_pas_struct (&tuxcmd_16_png[0], G_N_ELEMENTS (tuxcmd_16_png), "tuxcmd_16_png");
  dump_pas_struct (&tuxcmd_24_png[0], G_N_ELEMENTS (tuxcmd_24_png), "tuxcmd_24_png");
  dump_pas_struct (&tuxcmd_32_png[0], G_N_ELEMENTS (tuxcmd_32_png), "tuxcmd_32_png");
  dump_pas_struct (&tuxcmd_48_png[0], G_N_ELEMENTS (tuxcmd_48_png), "tuxcmd_48_png");
  dump_pas_struct (&tuxcmd_64_png[0], G_N_ELEMENTS (tuxcmd_64_png), "tuxcmd_64_png");
  dump_pas_struct (&tuxcmd_128_png[0], G_N_ELEMENTS (tuxcmd_128_png), "tuxcmd_128_png");

  return 0;
}

